#!/bin/bash
mkdir -p src
mkdir -p install
ORIGINAL_DIR=$(pwd)/src
INSTALL_DIR=$(pwd)/install

soft="autoconf-archive"
TARGET_INSTALL_DIR="$INSTALL_DIR/$soft"
SRC_DIR="$ORIGINAL_DIR/$soft"

cd "$ORIGINAL_DIR"

if [ ! -d "$TARGET_INSTALL_DIR" ]; then

    if [ ! -d "SRC_DIR" ]; then
	cd "$ORIGINAL_DIR";
	echo "Downloading $soft"
	wget https://mirror.cyberbits.eu/gnu/autoconf-archive/autoconf-archive-2023.02.20.tar.xz  > /dev/null || exit 1
	tar xf autoconf-archive-2023.02.20.tar.xz ||exit 1
	mv autoconf-archive-2023.02.20 "$SRC_DIR" || exit 1
	rm autoconf-archive-2023.02.20.tar.xz
    fi

    cd "$SRC_DIR"
    mkdir build
    cd build
    ../configure --prefix="$TARGET_INSTALL_DIR" &&make install
else
    echo "$soft is already installed"
fi
