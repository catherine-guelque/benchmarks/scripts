#!/bin/bash
mkdir -p src
mkdir -p install
ORIGINAL_DIR=$(pwd)/src
INSTALL_DIR=$(pwd)/install

cd "$ORIGINAL_DIR"


if [ ! -d "ZSTD" ]; then
  echo "Downloading ZSTD (required for Pilgrim and Pallas)"
  wget https://github.com/facebook/zstd/releases/download/v1.5.5/zstd-1.5.5.tar.gz > /dev/null
  tar -xvf zstd-1.5.5.tar.gz
  mv zstd-1.5.5 ZSTD
  rm -f zstd-1.5.5.tar.gz
fi
if [ ! -d "$INSTALL_DIR/ZSTD" ]; then
   echo "Installing ZSTD (required for Pilgrim and Pallas)"
   cd ZSTD
   cd build/cmake
   mkdir -p build && cd build
   cmake .. -DCMAKE_INSTALL_PREFIX:PATH="$INSTALL_DIR"/ZSTD 2>&1 | tee "$ORIGINAL_DIR"/zstd.config
   make -j 2>&1 | tee "$ORIGINAL_DIR"/zstd.build
   make install 2>&1 | tee "$ORIGINAL_DIR"/zstd.install
   cd "$ORIGINAL_DIR"
   #Move all the logs files
   echo "Moving the log files to logfiles/"
   mkdir -p logfiles
   mv ./*.config ./*.build ./*.install logfiles/
else
   echo "ZSTD already installed locally."
fi
load_in_path ZSTD

soft_name="ZSTD"
#Make the .env file
cd "$ORIGINAL_DIR"/..
rm -f ./$soft_name.env
echo "Creating the $(pwd)/$soft_name.env file."
dir="$INSTALL_DIR/$soft_name"

[ -d "$dir/bin" ] && echo "export PATH=\"$dir/bin:\$PATH\"" >> $soft_name.env
[ -d "$dir/lib" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib:\$LD_LIBRARY_PATH\"">> $soft_name.env
[ -d "$dir/lib32" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib32:\$LD_LIBRARY_PATH\"">> $soft_name.env
[ -d "$dir/lib64" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib64:\$LD_LIBRARY_PATH\"">> $soft_name.env

pkg_config_dir=$(find "$dir" -name "pkgconfig")
if [ -n "$pkg_config_dir" ]; then
  echo "export PKG_CONFIG_PATH=\"$pkg_config_dir:\$PKG_CONFIG_PATH\"" >> $soft_name.env
fi
