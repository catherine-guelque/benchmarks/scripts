#!/bin/bash

PREFIX=$(realpath $(dirname $0))

INSTALL_DIR=$PREFIX/install
SRC_DIR=$PREFIX/src

export PLATFORM=jeanzay

module purge
module load emacs cmake llvm/15.0.6 libtool/2.4.6 automake/1.16.1 autoconf/2.69 hwloc/2.7.1 libpciaccess/0.16 libxml2/2.9.9 pytorch-gpu/py3/2.3.0

# install OTF2
function install_otf2() {
    export OTF2_ROOT="$INSTALL_DIR/otf2"
    if [ ! -d "$OTF2_ROOT" ]; then
	OTF2_SRC_DIR="$SRC_DIR/otf2"
	if [ ! -d "$OTF2_SRC_DIR" ]; then
	    pushd "$SRC_DIR"
	    echo "Downloading OTF2 (required for EZTrace and StarPU)"
	    wget https://zenodo.org/records/7390099/files/otf2-3.0.2.tar.gz > /dev/null
	    tar -xvf otf2-3.0.2.tar.gz > /dev/null
	    rm -rf otf2-3.0.2.tar.gz
	    mv otf2-3.0.2 $OTF2_SRC_DIR
	    popd
	fi

	echo "Installing OTF2 (required for EZTrace)"
	cd "$OTF2_SRC_DIR"
	./configure -prefix="$OTF2_ROOT" 2>&1 | tee "$SRC_DIR"/otf2.config
	make -j 2>&1 | tee "$SRC_DIR"/otf2.build
	make install 2>&1 | tee "$SRC_DIR"/otf2.install
	cd "$SRC_DIR"
	#Move all the logs files
	echo "Moving the log files to logfiles/"
	mkdir -p logfiles
	mv ./*.config ./*.build ./*.install logfiles/    
    fi

    if [ -f "$OTF2_ROOT/bin/otf2-print" ]; then
	# installation was probably successful
	env_file="$PREFIX/otf2.env"
	echo "Creating the $env_file file."
	echo "#!/bin/bash" > "$env_file"

	echo "export OTF2_ROOT=$OTF2_ROOT" > "$env_file"
	echo "export PATH=\$OTF2_ROOT/bin:\$PATH" >> "$env_file"
	echo "export LD_LIBRARY_PATH=\$OTF2_ROOT/lib:\$LD_LIBRARY_PATH"  >> "$env_file"
	echo "export PKG_CONFIG_PATH=\$OTF2_ROOT/lib/pkgconfig:\$PKG_CONFIG_PATH" >> "$env_file"
    else
	echo "OTF2 installation failed"
    fi
}

function install_pallas() {
    export PALLAS_ROOT="$INSTALL_DIR/pallas"
    if [ ! -d "$PALLAS_ROOT" ]; then
	PALLAS_SRC_DIR="$SRC_DIR/pallas"
	if [ ! -d "$PALLAS_SRC_DIR" ]; then
	    pushd "$SRC_DIR"
	    echo "Downloading Pallas"
	    git clone https://gitlab.inria.fr/pallas/pallas.git > /dev/null
	    popd
	fi

	echo "Installing Pallas"
	cd "$PALLAS_SRC_DIR"
	BUILD_DIR="$PALLAS_SRC_DIR/build"
	PALLAS_COMPILE_MODE=RelWithDebInfo

	export PKG_CONFIG_PATH="$INSTALL_DIR/ZSTD/lib64/pkgconfig:$PKG_CONFIG_PATH"
	rm -rf "$BUILD_DIR" && mkdir -p "$BUILD_DIR" && cd $"$BUILD_DIR"
	cmake "$PALLAS_SRC_DIR" \
              -DBUILD_DOC=FALSE \
              -DCMAKE_INSTALL_PREFIX="$PALLAS_ROOT" \
              -DCMAKE_BUILD_TYPE="$PALLAS_COMPILE_MODE" \
              -DSZ_ROOT_DIR="$INSTALL_DIR"/SZ \
              -DZFP_ROOT_DIR="$INSTALL_DIR"/ZFP \
              2>&1 | tee "$SRC_DIR"/pallas.config
	make -j 16 2>&1 | tee "$SRC_DIR"/pallas.build
	make install 2>&1 | tee "$SRC_DIR"/pallas.install
	cd "$SRC_DIR"
    fi

    if [ -f "$PALLAS_ROOT/bin/pallas_print" ]; then
	# installation was probably successful
	env_file="$PREFIX/pallas.env"
	echo "Creating the $env_file file."
	echo "#!/bin/bash" > "$env_file"

	echo "export PALLAS_ROOT=$PALLAS_ROOT" > "$env_file"
	echo "export OTF2_ROOT=$PALLAS_ROOT" > "$env_file"
	echo "export PATH=\$PALLAS_ROOT/bin:\$PATH" >> "$env_file"
	echo "export LD_LIBRARY_PATH=\$PALLAS_ROOT/lib64:\$LD_LIBRARY_PATH"  >> "$env_file"
	echo "export PKG_CONFIG_PATH=\$PALLAS_ROOT/lib64/pkgconfig:\$PKG_CONFIG_PATH" >> "$env_file"
    else
	echo "Pallas installation failed"
    fi
}

function install_eztrace() {
    install_name=$1
    export EZTRACE_ROOT="$INSTALL_DIR/eztrace_${install_name}"

    if [ ! -d "$EZTRACE_ROOT" ]; then
	EZTRACE_SRC_DIR="$SRC_DIR/eztrace"
	EZTRACE_BUILD_DIR="$EZTRACE_SRC_DIR/build_${install_name}"
	if [ ! -d "$EZTRACE_SRC_DIR" ]; then
	    pushd "$SRC_DIR"
	    echo "Downloading EZTrace"
	    git clone -b dev https://gitlab.com/eztrace/eztrace.git > /dev/null
	    popd
	fi

	OLD_PATH=$PATH
	OLD_LD_LIBRARY_PATH=$LD_LIBRARY_PATH

	PATH=$OTF2_ROOT/bin:$PATH
	LD_LIBRARY_PATH=$OTF2_ROOT/lib64:$LD_LIBRARY_PATH

	echo "Installing EZTrace (version ${install_name})"
	rm -rf "$EZTRACE_BUILD_DIR"; mkdir -p "$EZTRACE_BUILD_DIR" && cd "$EZTRACE_BUILD_DIR"
	cmake "$EZTRACE_SRC_DIR" \
              -DEZTRACE_ENABLE_MPI=ON \
              -DEZTRACE_ENABLE_STARPU=OFF \
              -DEZTRACE_ENABLE_OMPT=OFF \
	      -DEZTRACE_ENABLE_CUDA=ON \
	      -DEZTRACE_ENABLE_PYTHON=ON \
              -DCMAKE_INSTALL_PREFIX="$EZTRACE_ROOT" \
              -DCMAKE_BUILD_TYPE=Release 2>&1 | tee "$SRC_DIR"/eztrace_${install_name}.config
	make -j 2>&1 | tee "$SRC_DIR"/eztrace_${install_name}.build
	make install 2>&1 | tee "$SRC_DIR"/eztrace_${install_name}.install
	cd "$SRC_DIR"

	PATH=$OLD_PATH
	LD_LIBRARY_PATH=$OLD_LD_LIBRARY_PATH
    fi

    if [ -f "$EZTRACE_ROOT/bin/eztrace" ]; then
	# installation was probably successful
	env_file="$PREFIX/eztrace_${install_name}.env"
	echo "Creating the $env_file file."
	echo "#!/bin/bash" > "$env_file"

	echo "export OTF2_ROOT=$OTF2_ROOT" > "$env_file"
	echo "export EZTRACE_ROOT=$EZTRACE_ROOT" > "$env_file"
	echo "export PATH=\$OTF2_ROOT/bin:\$EZTRACE_ROOT/bin:\$PATH" >> "$env_file"
	echo "export LD_LIBRARY_PATH=\$EZTRACE_ROOT/lib64:\$LD_LIBRARY_PATH"  >> "$env_file"
	echo "export PKG_CONFIG_PATH=\$OTF2_ROOT/lib64/pkgconfig:\$EZTRACE_ROOT/lib64/pkgconfig:\$PKG_CONFIG_PATH" >> "$env_file"
    else
	echo "EZTrace ${install_name} installation failed"
    fi
}

install_otf2
source "$PREFIX/otf2.env"
install_eztrace pytorch_otf2

install_pallas
source "$PREFIX/pallas.env"
install_eztrace pytorch_pallas
