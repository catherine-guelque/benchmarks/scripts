#!/bin/bash
mkdir -p src
mkdir -p install
ORIGINAL_DIR=$(pwd)/src
INSTALL_DIR=$(pwd)/install

function load_in_path {
  if [ -f "$ORIGINAL_DIR/../$1.env" ]; then
    source "$ORIGINAL_DIR/../$1.env"
  else
    export LD_LIBRARY_PATH="$INSTALL_DIR/$1/lib:$LD_LIBRARY_PATH";
    export PATH="$INSTALL_DIR/$1/bin:$PATH";
    pkg_config_dir=$(find "$INSTALL_DIR/$1" -name "pkgconfig")
    if [ -n "$pkg_config_dir" ]; then
	    export PKG_CONFIG_PATH="$pkg_config_dir:$PKG_CONFIG_PATH"
    fi
  fi
}

#Clean previous installs
rm -rf "$INSTALL_DIR/eztrace_pallas" "$INSTALL_DIR/pallas"
cd "$ORIGINAL_DIR"

if ! which zstd > /dev/null 2>&1; then
  load_in_path ZSTD
fi
if ! which sz > /dev/null 2>&1; then
  load_in_path SZ
fi
if ! which zfp > /dev/null 2>&1; then
  load_in_path ZFP
fi


## opari2 (for eztrace OpenMP)
#if [ ! -d "opari" ]; then
#    echo "Downloading Opari"
#    wget https://perftools.pages.jsc.fz-juelich.de/cicd/opari2/tags/opari2-2.0.8/opari2-2.0.8.tar.gz > /dev/null
#    tar xf opari2-2.0.8.tar.gz
#    mv opari2-2.0.8 opari
#fi
#if [ ! -d "$INSTALL_DIR"/opari ]; then
#    echo "Installing Opari"
#    cd opari
#    rm -rf build && mkdir -p build && cd build
#    ../configure --prefix="$INSTALL_DIR"/opari 2>&1 | tee "$ORIGINAL_DIR"/eztrace_otf2.config
#    make -j 2>&1 | tee "$ORIGINAL_DIR"/opari.build
#    make install 2>&1 | tee "$ORIGINAL_DIR"/opari.install
#    cd "$ORIGINAL_DIR"
#fi

if [ "$USE_STARPU" = "ON" ]; then
  load_in_path starpu
fi
if [ "$USE_MPICH" = "ON" ]; then
  load_in_path mpich
fi
load_in_path otf2
load_in_path opari

#ezTrace OTF2
if [ ! -d "eztrace" ]; then
    echo "Downloading EZTrace"
    git clone https://gitlab.com/eztrace/eztrace.git > /dev/null
fi
if [ ! -d "$INSTALL_DIR"/eztrace_otf2 ]; then
    echo "Installing EZTrace"
    cd eztrace
    rm -rf build_otf2 && mkdir -p build_otf2 && cd build_otf2
    cmake .. \
	  -DCMAKE_C_COMPILER=$CC\
          -DEZTRACE_ENABLE_MPI=ON \
          -DEZTRACE_ENABLE_STARPU="$USE_STARPU" \
          -DEZTRACE_ENABLE_OMPT="$USE_OMPT" \
	        -DEZTRACE_ENABLE_CUDA="$USE_CUDA" \
          -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR"/eztrace_otf2 \
          -DCMAKE_BUILD_TYPE=Release 2>&1 | tee "$ORIGINAL_DIR"/eztrace_otf2.config
    make -j 2>&1 | tee "$ORIGINAL_DIR"/eztrace_otf2.build
    make install 2>&1 | tee "$ORIGINAL_DIR"/eztrace_otf2.install
    cd "$ORIGINAL_DIR"
fi


if [ ! -d "pallas" ]; then
    echo "Downloading Pallas"
    git clone https://gitlab.inria.fr/pallas/pallas.git > /dev/null
fi
if [ ! -d "$INSTALL_DIR/pallas" ]; then
    echo "Installing Pallas"
    cd pallas
    rm -rf build && mkdir -p build && cd build
    cmake .. \
        -DBUILD_DOC=FALSE \
        -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR"/pallas \
        -DCMAKE_BUILD_TYPE="$PALLAS_COMPILE_MODE" \
        -DSZ_ROOT_DIR="$INSTALL_DIR"/SZ \
        -DZFP_ROOT_DIR="$INSTALL_DIR"/ZFP \
        2>&1 | tee "$ORIGINAL_DIR"/pallas.config
    make -j 16 2>&1 | tee "$ORIGINAL_DIR"/pallas.build
    make install 2>&1 | tee "$ORIGINAL_DIR"/pallas.install
    cd "$ORIGINAL_DIR"

    load_in_path pallas

    if [ -d "$INSTALL_DIR/pallas" ] && [ ! -d "$INSTALL_DIR"/eztrace_pallas ]; then
        echo "Installing EZTrace with Pallas"
        cd eztrace
        rm -rf build_pallas && mkdir -p build_pallas && cd build_pallas
        cmake .. \
  	      -DCMAKE_C_COMPILER=$CC\
              -DEZTRACE_ENABLE_MPI=ON \
              -DEZTRACE_ENABLE_STARPU="$USE_STARPU" \
              -DEZTRACE_ENABLE_OMPT="$USE_OMPT" \
	            -DEZTRACE_ENABLE_CUDA="$USE_CUDA" \
              -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR"/eztrace_pallas \
              -DCMAKE_BUILD_TYPE="$PALLAS_COMPILE_MODE" 2>&1 | tee "$ORIGINAL_DIR"/eztrace_pallas.config
        make -j 16 2>&1 | tee "$ORIGINAL_DIR"/eztrace_pallas.build
        make install 2>&1 | tee "$ORIGINAL_DIR"/eztrace_pallas.install
        cd "$ORIGINAL_DIR"
    fi
fi

#Move all the logs files
echo "Moving the log files to logfiles/"
mkdir -p logfiles
mv ./*.config ./*.build ./*.install logfiles/

#Make the .env file
cd "$ORIGINAL_DIR"/..
rm -f eztrace.env pallas.env

echo "Creating the $(pwd)/eztrace_otf2.env file."
if [ -d "$INSTALL_DIR/eztrace_otf2" ]; then
  echo "# eztrace_otf2"
  echo "export PATH=\"$INSTALL_DIR/eztrace_otf2/bin:\$PATH\""
  echo "export LD_LIBRARY_PATH=\"$INSTALL_DIR/eztrace_otf2/lib[\"32\"\"64\"]?:\$LD_LIBRARY_PATH\""
  pkg_config_dir=$(find "$INSTALL_DIR/eztrace_otf2" -name "pkgconfig")
  if [ -n "$pkg_config_dir" ]; then
    echo "export PKG_CONFIG_PATH=\"$pkg_config_dir:\$PKG_CONFIG_PATH\""
  fi
fi >> eztrace.env

echo "Creating the $(pwd)/pallas.env file."
if [ -d "$INSTALL_DIR/pallas" ]; then
    echo "# eztrace_pallas"
    dir="$INSTALL_DIR/eztrace_pallas"
    echo "export PATH=\"$dir/bin:\$PATH\""
    [ -d "$dir/lib" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib:\$LD_LIBRARY_PATH\""
    [ -d "$dir/lib32" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib32:\$LD_LIBRARY_PATH\""
    [ -d "$dir/lib64" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib64:\$LD_LIBRARY_PATH\""
    pkg_config_dir=$(find "$INSTALL_DIR/eztrace_pallas" -name "pkgconfig")
    if [ -n "$pkg_config_dir" ]; then
      echo "export PKG_CONFIG_PATH=\"$pkg_config_dir:\$PKG_CONFIG_PATH\""
    fi
    echo ""
    echo "# pallas"
    dir="$INSTALL_DIR/pallas"
    echo "export PATH=\"$dir/bin:\$PATH\""
    [ -d "$dir/lib" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib:\$LD_LIBRARY_PATH\""
    [ -d "$dir/lib32" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib32:\$LD_LIBRARY_PATH\""
    [ -d "$dir/lib64" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib64:\$LD_LIBRARY_PATH\""
    pkg_config_dir=$(find "$INSTALL_DIR/pallas" -name "pkgconfig")
    if [ -n "$pkg_config_dir" ]; then
      echo "export PKG_CONFIG_PATH=\"$pkg_config_dir:\$PKG_CONFIG_PATH\""
    fi
fi >> pallas.env
