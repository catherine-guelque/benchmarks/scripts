# Software required for benchmarking
Here are some scripts to install everything needed for benchmarking.
You might not need to use all of these.
 - `installBase.sh`: Installs ZSTD, MPICH, OTF2, Bison and JsonCPP. Run this first, then run `source base_software.env` before running the rest.
 - `installStarpu.sh`: Installs StarPU and Chameleon.
 - `installScalatrace.sh`: Installs ScalaTrace.
 - `installPilgrim.sh`: Installs Pilgrim.
 - `installPallas.sh`: Installs EZTrace, Pallas, and a version of EZTrace that uses Pallas rather than OTF2.

## Using the env files
Each of these script will generate a `.env` file. This file will add some folders to your PATH, LD_LIBRARY_PATH and PKG_CONFIG_PATH.
It's up to you to source these when you want.

## Using GUIX
If you use GUIX, you can run `guix shell -m guix.manifest` to skip the installation
of the base software.