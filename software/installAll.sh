#!/bin/bash
export USE_MPICH="ON"
export USE_STARPU="ON"
export USE_CUDA="ON"
export USE_OMPT="ON"
export PALLAS_COMPILE_MODE="Release"

for arg in "$@"; do
  if [ "$arg" = "--no-mpich" ]; then
    export USE_MPICH="OFF"
  elif [ "$arg" = "--no-starpu" ]; then
    export USE_STARPU="OFF"
  elif [ "$arg" = "--no-cuda" ]; then
    export USE_CUDA="OFF"
  elif [ "$arg" = "--no-ompt" ]; then
    export USE_OMPT="OFF"
  elif [ "$arg" = "-D" ]; then
    export PALLAS_COMPILE_MODE="Debug"
  else
    echo "Command argument not recognized: $arg."
    exit 1
  fi
done

if [ "$CC" = "" ]; then
  export CC="gcc"
fi

if [ "$USE_MPICH" = "ON" ]; then
  ./installMpich.sh
fi
./installZSTD.sh
./installSZ.sh
./installZFP.sh
./installScalatrace.sh
if [ "$USE_STARPU" = "ON" ]; then
  ./installStarpu.sh
fi
./installPallas.sh
./installPilgrim.sh
