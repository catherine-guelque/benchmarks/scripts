#!/bin/bash
mkdir -p src
mkdir -p install
ORIGINAL_DIR=$(pwd)/src
INSTALL_DIR=$(pwd)/install

STARPU_COMMIT=2dee8cfc502c6de7c871fde8d76a6f477c06a239
CHAMELON_COMMIT=d3135e9b8f2223b686c47a13f7bdc3a00d9268d1

function load_in_path {
  if [ -f "$ORIGINAL_DIR/../$1.env" ]; then
    source "$ORIGINAL_DIR/../$1.env"
  else
    export LD_LIBRARY_PATH="$INSTALL_DIR/$1/lib:$LD_LIBRARY_PATH";
    export PATH="$INSTALL_DIR/$1/bin:$PATH";
    pkg_config_dir=$(find "$INSTALL_DIR/$1" -name "pkgconfig")
    if [ -n "$pkg_config_dir" ]; then
	    export PKG_CONFIG_PATH="$pkg_config_dir:$PKG_CONFIG_PATH"
    fi
  fi
}

cd "$ORIGINAL_DIR"
if [ "$USE_MPICH" = "ON" ]; then
  load_in_path mpich
fi
if [ ! -d "starpu" ]; then
    echo "Downloading StarPU"
    git clone https://gitlab.inria.fr/starpu/starpu.git > /dev/null
    cd starpu
    git checkout $STARPU_COMMIT
    cd "$ORIGINAL_DIR"
fi
if [ ! -d "$INSTALL_DIR/starpu" ]; then
    echo "Installing StarPU"
    cd starpu
    ./autogen.sh > /dev/null
    mkdir -p build && cd build
    ../configure --prefix="$INSTALL_DIR"/starpu \
		 --disable-opencl \
		 --enable-cuda \
		 --disable-hip \
		 --disable-fortran \
		 NVCCFLAGS=-allow-unsupported-compiler \
		 2>&1 | tee "$ORIGINAL_DIR"/starpu.config
    make -j 2>&1 | tee "$ORIGINAL_DIR"/starpu.build
    make install 2>&1 | tee "$ORIGINAL_DIR"/starpu.install
    cd "$ORIGINAL_DIR"
    #Move all the logs files
    echo "Moving the log files to logfiles/"
    mkdir -p logfiles
    mv ./*.config ./*.build ./*.install logfiles/
else
    echo "StarPU already installed."
fi
load_in_path starpu


#Chameleon
if [ ! -d "chameleon" ]; then
    echo "Downloading Chameleon"
    git clone --recurse-submodules https://gitlab.inria.fr/solverstack/chameleon.git
    cd chameleon
    git checkout $CHAMELON_COMMIT
    cd "$ORIGINAL_DIR"
fi
if [ ! -d "$INSTALL_DIR/chameleon" ]; then
    echo "Installing Chameleon"
    cd chameleon && mkdir -p build && cd build
    cmake .. \
        -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR"/chameleon/ \
        -DBUILD_SHARED_LIBS=ON \
        -DCHAMELEON_USE_MPI=ON \
        -DCHAMELEON_USE_CUDA="$USE_CUDA" \
        -DCHAMELEON_USE_MPI_DATATYPES=ON \
        -DCHAMELEON_ENABLE_EXAMPLE=OFF \
        -DCHAMELEON_ENABLE_TESTING=OFF \
        -DCHAMELEON_PREC_C=OFF \
        -DCHAMELEON_PREC_Z=OFF \
        -DBLA_VENDOR=Intel10_64lp_seq \
        2>&1 | tee "$ORIGINAL_DIR"/chameleon.config
    make -j 2>&1 | tee "$ORIGINAL_DIR"/chameleon.build
    make install 2>&1 | tee "$ORIGINAL_DIR"/chameleon.install
    cd "$ORIGINAL_DIR"
    #Move all the logs files
    echo "Moving the log files to logfiles/"
    mkdir -p logfiles
    mv ./*.config ./*.build ./*.install logfiles/
fi

#Make the .env file
cd "$ORIGINAL_DIR"/..
rm -f starpu.env

echo "Creating the $(pwd)/starpu.env file."
if [ -d "$INSTALL_DIR/starpu" ]; then
    echo "# starpu"
    echo "export PATH=\"$INSTALL_DIR/starpu/bin:\$PATH\""
    echo "export LD_LIBRARY_PATH=\"$INSTALL_DIR/starpu/lib[\"32\"\"64\"]?:\$LD_LIBRARY_PATH\""
    echo "export PKG_CONFIG_PATH=\"$INSTALL_DIR/starpu/lib[\"32\"\"64\"]?/pkgconfig:\$PKG_CONFIG_PATH\""
    echo ""
    echo "# chameleon"
    echo "export PATH=\"$INSTALL_DIR/chameleon/bin:\$PATH\""
    echo "export LD_LIBRARY_PATH=\"$INSTALL_DIR/chameleon/lib[\"32\"\"64\"]?:\$LD_LIBRARY_PATH\""
    echo "export PKG_CONFIG_PATH=\"$INSTALL_DIR/chameleon/lib[\"32\"\"64\"]?/pkgconfig:\$PKG_CONFIG_PATH\""
    echo ""
fi >> starpu.env
