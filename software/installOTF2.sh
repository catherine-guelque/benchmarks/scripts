#!/bin/bash
mkdir -p src
mkdir -p install
ORIGINAL_DIR=$(pwd)/src
INSTALL_DIR=$(pwd)/install

cd "$ORIGINAL_DIR"

if [ ! -d "otf2" ]; then
    echo "Downloading OTF2 (required for EZTrace and StarPU)"
    wget https://zenodo.org/records/7390099/files/otf2-3.0.2.tar.gz > /dev/null
    tar -xvf otf2-3.0.2.tar.gz > /dev/null
    rm -rf otf2-3.0.2.tar.gz
    mv otf2-3.0.2 otf2
fi
if [ ! -d "$INSTALL_DIR/otf2" ]; then
    echo "Installing OTF2 (required for EZTrace and StarPU)"
    cd otf2
    ./configure -prefix="$INSTALL_DIR"/otf2 2>&1 | tee "$ORIGINAL_DIR"/otf2.config
    make -j 2>&1 | tee "$ORIGINAL_DIR"/otf2.build
    make install 2>&1 | tee "$ORIGINAL_DIR"/otf2.install
    cd "$ORIGINAL_DIR"
    #Move all the logs files
    echo "Moving the log files to logfiles/"
    mkdir -p logfiles
    mv ./*.config ./*.build ./*.install logfiles/
else
    echo "OTF2 already installed."
fi

soft_name="otf2"

#Make the .env file
cd "$ORIGINAL_DIR"/..
rm -f ./$soft_name.env
echo "Creating the $(pwd)/$soft_name.env file."
dir="$INSTALL_DIR/$soft_name"
[ -d "$dir/bin" ] && echo "export PATH=\"$dir/bin:\$PATH\"" >> $soft_name.env
[ -d "$dir/lib" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib:\$LD_LIBRARY_PATH\"" >> $soft_name.env
[ -d "$dir/lib32" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib32:\$LD_LIBRARY_PATH\"" >> $soft_name.env
[ -d "$dir/lib64" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib64:\$LD_LIBRARY_PATH\"" >> $soft_name.env
pkg_config_dir=$(find "$dir" -name "pkgconfig")
if [ -n "$pkg_config_dir" ]; then
  echo "export PKG_CONFIG_PATH=\"$pkg_config_dir:\$PKG_CONFIG_PATH\"" >> $soft_name.env
fi

