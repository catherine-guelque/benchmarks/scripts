#!/bin/bash
mkdir -p src
mkdir -p install
ORIGINAL_DIR=$(pwd)/src
INSTALL_DIR=$(pwd)/install
SCRIPTS_DIR=$(pwd)

PATCH_PILGRIM="$SCRIPTS_DIR/patch_pilgrim"

function load_in_path {
  if [ -f "$ORIGINAL_DIR/../$1.env" ]; then
    source "$ORIGINAL_DIR/../$1.env"
  else
    export LD_LIBRARY_PATH="$INSTALL_DIR/$1/lib:$LD_LIBRARY_PATH";
    export PATH="$INSTALL_DIR/$1/bin:$PATH";
    pkg_config_dir=$(find "$INSTALL_DIR/$1" -name "pkgconfig")
    if [ -n "$pkg_config_dir" ]; then
	    export PKG_CONFIG_PATH="$pkg_config_dir:$PKG_CONFIG_PATH"
    fi
  fi
}

cd "$ORIGINAL_DIR"

if ! which zstd > /dev/null 2>&1; then
  load_in_path ZSTD
fi
if ! which sz > /dev/null 2>&1; then
  load_in_path SZ
fi
if ! which zfp > /dev/null 2>&1; then
  load_in_path ZFP
fi
if [ "$USE_MPICH" = "ON" ]; then
  load_in_path mpich
fi

if [ ! -d "pilgrim" ]; then
    echo "Downloading Pilgrim"
    git clone https://github.com/valentinhon/pilgrim > /dev/null

    # On Jean-Zayn we may have to patch pilgrim to use our own autoconf-archive
    if [ "$PLATFORM" = "jeanzay" ]; then
	    cd pilgrim
	    patch -p1 < "$PATCH_PILGRIM"
	    cd -
    fi
fi
if [ ! -d "$INSTALL_DIR/pilgrim" ]; then
    echo "Installing Pilgrim"
    cd pilgrim
    #    ./autogen.sh > /dev/null
    autoreconf -vfi
    ./configure --enable-pointers --enable-tid \
		CC=mpicc \
        CFLAGS="$(pkg-config --cflags-only-I libzstd) $(pkg-config --libs-only-L libzstd)" \
        --with-sz="$INSTALL_DIR"/SZ \
        --with-zfp="$INSTALL_DIR"/ZFP 2>&1 | tee "$ORIGINAL_DIR"/pilgrim.config
    make -j 2>&1 | tee "$ORIGINAL_DIR"/pilgrim.build
    make install prefix="$INSTALL_DIR"/pilgrim 2>&1 | tee "$ORIGINAL_DIR"/pilgrim.install
    cd "$ORIGINAL_DIR"
    #Move all the logs files
    echo "Moving the log files to logfiles/"
    mkdir -p logfiles
    mv ./*.config ./*.build ./*.install logfiles/
else
    echo "Pilgrim already installed."
fi

#Make the .env file
cd "$ORIGINAL_DIR"/..
rm -f pilgrim.env

echo "Creating the $(pwd)/pilgrim.env file."
if [ -d "$INSTALL_DIR/pilgrim" ]; then
  echo "export PILGRIM_PATH=\"${INSTALL_DIR}/pilgrim\""
  echo "export PATH=\"${INSTALL_DIR}/pilgrim/bin:\$PATH\""
fi >> pilgrim.env
