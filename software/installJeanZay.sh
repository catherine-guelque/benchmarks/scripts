#!/bin/bash
cd /lustre/fswork/projects/rech/avz/commun/scripts/software/
source jeanzay.env

export USE_MPICH="OFF"
export USE_STARPU="ON"
export USE_CUDA="ON"
export USE_OMPT="ON"
export PALLAS_COMPILE_MODE="Release"

./installZSTD.sh
./installSZ.sh
./installZFP.sh
./installScalatrace.sh
./installOTF2.sh
./installOTF2Analysis.sh
./installPallas.sh
./installPilgrim.sh
./installPallasAnalysis.sh

#for soft in ZSTD SZ ZFP Scalatrace Pallas Pilgrim; do
#  temp="${soft}_STATUS"
#  echo "${!temp}"
#  if [ "${!temp}" = "0" ]; then
#    echo -e "$soft status: \033[32m Installation successful\033[0m"
#  else
#    echo -e "$soft status: \033[31m Installation failed\033[0m"
#  fi
#done
