#!/bin/bash
mkdir -p src
mkdir -p install
ORIGINAL_DIR=$(pwd)/src
INSTALL_DIR=$(pwd)/install

function load_in_path {
  if [ -f "$ORIGINAL_DIR/../$1.env" ]; then
    source "$ORIGINAL_DIR/../$1.env"
  else
    export LD_LIBRARY_PATH="$INSTALL_DIR/$1/lib:$LD_LIBRARY_PATH";
    export PATH="$INSTALL_DIR/$1/bin:$PATH";
    pkg_config_dir=$(find "$INSTALL_DIR/$1" -name "pkgconfig")
    if [ -n "$pkg_config_dir" ]; then
	    export PKG_CONFIG_PATH="$pkg_config_dir:$PKG_CONFIG_PATH"
    fi
  fi
}

#Clean previous installs
#rm -rf "$INSTALL_DIR/pallas-contention" "$INSTALL_DIR/pallas-comm-matrix"
cd "$ORIGINAL_DIR"

load_in_path pallas


if [ ! -d "pallas-contention" ]; then
    echo "Downloading Pallas Contention"
    git clone https://gitlab.inria.fr/pallas/analysis-tools/pallas-contention.git > /dev/null
fi


if [ ! -d "$INSTALL_DIR/pallas-contention" ]; then
    echo "Installing Pallas Contention"
    cd pallas-contention
    rm -rf build && mkdir -p build && cd build
    cmake .. \
        -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR"/pallas-contention \
        2>&1 | tee "$ORIGINAL_DIR"/pallas-contention.config
    make -j 16 2>&1 | tee "$ORIGINAL_DIR"/pallas-contention.build
    make install 2>&1 | tee "$ORIGINAL_DIR"/pallas-contention.install
    cd "$ORIGINAL_DIR"
fi

if [ ! -d "pallas-communication-matrix" ]; then
    echo "Downloading Pallas Communication Matrix"
    git clone https://gitlab.inria.fr/pallas/analysis-tools/pallas-communication-matrix.git > /dev/null
fi


if [ ! -d "$INSTALL_DIR/pallas-communication-matrix" ]; then
    echo "Installing Pallas Communication Matrix"
    cd pallas-communication-matrix
    rm -rf build && mkdir -p build && cd build
    cmake .. \
        -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR"/pallas-communication-matrix \
        2>&1 | tee "$ORIGINAL_DIR"/pallas-communication-matrix.config
    make -j 16 2>&1 | tee "$ORIGINAL_DIR"/pallas-communication-matrix.build
    make install 2>&1 | tee "$ORIGINAL_DIR"/pallas-communication-matrix.install
    cd "$ORIGINAL_DIR"
fi


#Move all the logs files
echo "Moving the log files to logfiles/"
mkdir -p logfiles
mv ./*.config ./*.build ./*.install logfiles/

#Make the .env file
cd "$ORIGINAL_DIR"/..

echo "Creation pallas_analysis.env" 
if [ -d "$INSTALL_DIR/pallas-contention" ]; then
    echo "# Pallas contention"
    echo "export PATH=\"$INSTALL_DIR/pallas-contention/bin:\$PATH\""
fi >> pallas.env

if [ -d "$INSTALL_DIR/pallas-communication-matrix" ]; then
    echo "# Pallas Communication Matrix"
    echo "export PATH=\"$INSTALL_DIR/pallas-communication-matrix/bin:\$PATH\""
fi >> pallas.env
