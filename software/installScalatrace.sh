#!/bin/bash
mkdir -p src
mkdir -p install
ORIGINAL_DIR=$(pwd)/src
INSTALL_DIR=$(pwd)/install

cd "$ORIGINAL_DIR"

function load_in_path {
  if [ -f "$ORIGINAL_DIR/../$1.env" ]; then
    source "$ORIGINAL_DIR/../$1.env"
  else
    export LD_LIBRARY_PATH="$INSTALL_DIR/$1/lib:$LD_LIBRARY_PATH";
    export PATH="$INSTALL_DIR/$1/bin:$PATH";
    pkg_config_dir=$(find "$INSTALL_DIR/$1" -name "pkgconfig")
    if [ -n "$pkg_config_dir" ]; then
	    export PKG_CONFIG_PATH="$pkg_config_dir:$PKG_CONFIG_PATH"
    fi
  fi
}

if [ "$USE_MPICH" = "ON" ]; then
  load_in_path mpich
fi

if [ ! -d "scalatrace" ]; then
    echo "Downloading ScalaTrace"
    git clone https://gitlab.inf.telecom-sudparis.eu/catherine-guelque/benchmarks/scalatrace.git > /dev/null
fi
if [ ! -d "$INSTALL_DIR/scalatrace" ]; then
    echo "Installing ScalaTrace"
    cd scalatrace
    make 2>&1 | tee "$ORIGINAL_DIR"/scalatrace.build
    mkdir -p "$INSTALL_DIR"/scalatrace/bin "$INSTALL_DIR"/scalatrace/lib
    cp -r replay/replay scalatrace_print/scalatrace_print "$INSTALL_DIR"/scalatrace/bin
    cp -r record/lib/* stackwalk/ver0/* "$INSTALL_DIR"/scalatrace/lib
    cd "$ORIGINAL_DIR"
    #Move all the logs files
    echo "Moving the log files to logfiles/"
    mkdir -p logfiles
    mv ./*.config ./*.build ./*.install logfiles/
else
    echo "ScalaTrace already installed"
fi

cd "$ORIGINAL_DIR"/..
rm -f scalatrace.env

echo "Creating the $(pwd)/scalatrace.env file."
if [ -d "$INSTALL_DIR/scalatrace" ]; then
  echo "# scalatrace"
  echo "export PATH=\"$INSTALL_DIR/scalatrace/bin:\$PATH\""
  [ -d "$dir/lib" ] && echo "export LD_LIBRARY_PATH=\"$INSTALL_DIR/scalatrace/lib:\$LD_LIBRARY_PATH\""
  [ -d "$dir/lib32" ] && echo "export LD_LIBRARY_PATH=\"$INSTALL_DIR/scalatrace/lib32:\$LD_LIBRARY_PATH\""
  [ -d "$dir/lib64" ] && echo "export LD_LIBRARY_PATH=\"$INSTALL_DIR/scalatrace/lib64:\$LD_LIBRARY_PATH\""
fi >> scalatrace.env
