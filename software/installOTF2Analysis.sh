#!/bin/bash
mkdir -p src
mkdir -p install
ORIGINAL_DIR=$(pwd)/src
INSTALL_DIR=$(pwd)/install

function load_in_path {
  if [ -f "$ORIGINAL_DIR/../$1.env" ]; then
    source "$ORIGINAL_DIR/../$1.env"
  else
    export LD_LIBRARY_PATH="$INSTALL_DIR/$1/lib:$LD_LIBRARY_PATH";
    export PATH="$INSTALL_DIR/$1/bin:$PATH";
    pkg_config_dir=$(find "$INSTALL_DIR/$1" -name "pkgconfig")
    if [ -n "$pkg_config_dir" ]; then
	    export PKG_CONFIG_PATH="$pkg_config_dir:$PKG_CONFIG_PATH"
    fi
  fi
}

#Clean previous installs
#rm -rf "$INSTALL_DIR/otf2-communication-matrix" "$INSTALL_DIR/otf2-profile"
cd "$ORIGINAL_DIR"

load_in_path otf2

if [ ! -d "otf2-communication-matrix" ]; then
    echo "Downloading OTF2 communication matrix"
    git clone https://gitlab.com/eztrace/analysis-tools/otf2-communication-matrix.git > /dev/null
fi

if [ ! -d "$INSTALL_DIR/otf2-communication-matrix" ]; then
    echo "Installing OTF2 Communication Matrix"
    cd otf2-communication-matrix
    rm -rf build && mkdir -p build && cd build
    cmake .. \
        -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR"/otf2-communication-matrix \
        2>&1 | tee "$ORIGINAL_DIR"/otf2-communication-matrix.config
    make -j 16 2>&1 | tee "$ORIGINAL_DIR"/otf2-communication-matrix.build
    make install 2>&1 | tee "$ORIGINAL_DIR"/otf2-communication-matrix.install
    cd "$ORIGINAL_DIR"
fi


if [ ! -d "otf2-profile" ]; then
    echo "Downloading OTF2 profile"
    git clone https://gitlab.com/eztrace/analysis-tools/otf2-profile.git > /dev/null
fi

if [ ! -d "$INSTALL_DIR/otf2-profile" ]; then
    echo "Installing OTF2 Profiling tool"
    cd otf2-profile
    rm -rf build && mkdir -p build && cd build
    cmake .. \
        -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR"/otf2-profile \
        2>&1 | tee "$ORIGINAL_DIR"/otf2-profile.config
    make -j 16 2>&1 | tee "$ORIGINAL_DIR"/otf2-profile.build
    make install 2>&1 | tee "$ORIGINAL_DIR"/otf2-profile.install
    mv  "$INSTALL_DIR"/otf2-profile/bin/otf2_profile "$INSTALL_DIR"/otf2-profile/bin/otf2_contention 
    cd "$ORIGINAL_DIR"
fi




#Move all the logs files
echo "Moving the log files to logfiles/"
mkdir -p logfiles
mv ./*.config ./*.build ./*.install logfiles/

#Make the .env file
cd "$ORIGINAL_DIR"/..

echo "Creating otf2.env"
if [ -d "$INSTALL_DIR/otf2-communication-matrix" ]; then
    echo "# OTF2 Communication Matrix"
    echo "export PATH=\"$INSTALL_DIR/otf2-communication-matrix/bin:\$PATH\""
fi >> otf2.env

if [ -d "$INSTALL_DIR/otf2-profile" ]; then
    echo "# OTF2 Profiling tool"
    echo "export PATH=\"$INSTALL_DIR/otf2-profile/bin:\$PATH\""
fi >> otf2.env
