#!/bin/bash
mkdir -p src
mkdir -p install
ORIGINAL_DIR=$(pwd)/src
INSTALL_DIR=$(pwd)/install

cd "$ORIGINAL_DIR"

if ! which zstd > /dev/null 2>&1; then
  source ../ZSTD.env
fi

if ! which sz  > /dev/null 2>&1 ; then
  if [ ! -d "SZ" ]; then
      echo "Downloading SZ (required for Pilgrim and Pallas)"
      git clone https://github.com/szcompressor/SZ.git > /dev/null
  fi
  if [ ! -d "$INSTALL_DIR/SZ" ]; then
      echo "Installing SZ (required for Pilgrim and Pallas)"
      cd SZ
      git checkout v2.1.9
      mkdir -p build && cd build
      cmake .. -DCMAKE_INSTALL_PREFIX:PATH="$INSTALL_DIR"/SZ 2>&1 | tee "$ORIGINAL_DIR"/sz.config
      make -j 2>&1 | tee "$ORIGINAL_DIR"/sz.build
      make install 2>&1 | tee "$ORIGINAL_DIR"/sz.install
      cd "$ORIGINAL_DIR"
      #Move all the logs files
      echo "Moving the log files to logfiles/"
      mkdir -p logfiles
      mv ./*.config ./*.build ./*.install logfiles/
  else
      echo "SZ already installed locally."
  fi
else
      echo "SZ already installed globally."
fi

#Make the .env file
soft_name="SZ"
cd "$ORIGINAL_DIR"/..
rm -f ./$soft_name.env
echo "Creating the $(pwd)/$soft_name.env file."
dir="$INSTALL_DIR/$soft_name"

[ -d "$dir/bin" ] && echo "export PATH=\"$dir/bin:\$PATH\"" >> $soft_name.env
[ -d "$dir/lib" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib:\$LD_LIBRARY_PATH\"">> $soft_name.env
[ -d "$dir/lib32" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib32:\$LD_LIBRARY_PATH\"">> $soft_name.env
[ -d "$dir/lib64" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib64:\$LD_LIBRARY_PATH\"">> $soft_name.env

pkg_config_dir=$(find "$dir" -name "pkgconfig")
if [ -n "$pkg_config_dir" ]; then
  echo "export PKG_CONFIG_PATH=\"$pkg_config_dir:\$PKG_CONFIG_PATH\"" >> $soft_name.env
fi

