#!/bin/bash
mkdir -p src
mkdir -p install
ORIGINAL_DIR=$(pwd)/src
INSTALL_DIR=$(pwd)/install

# prevent MPICH from spawning 3000+ gcc processes
make_parallelism="-j $(( $(nproc) * 10 ))"
cd "$ORIGINAL_DIR"

#MPICH
if [ ! -d "mpich" ]; then
    echo "Downloading mpich"
    wget https://github.com/pmodels/mpich/releases/download/v4.0.1/mpich-4.0.1.tar.gz > /dev/null
    tar -xvf mpich-4.0.1.tar.gz > /dev/null
    rm -rf mpich-4.0.1.tar.gz
    mv mpich-4.0.1 mpich
fi
if [ ! -d "$INSTALL_DIR/mpich" ]; then
    echo "Installing mpich"
    cd mpich
    FFLAGS=-fallow-argument-mismatch \
    FCFLAGS=-fallow-argument-mismatch \
    ./configure --prefix="$INSTALL_DIR"/mpich 2>&1 | tee "$ORIGINAL_DIR"/mpich.config
    make $make_parallelism 2>&1 | tee "$ORIGINAL_DIR"/mpich.build
    make install 2>&1 | tee "$ORIGINAL_DIR"/mpich.install
    cd "$ORIGINAL_DIR"
    #Move all the logs files
    echo "Moving the log files to logfiles/"
    mkdir -p logfiles
    mv ./*.config ./*.build ./*.install logfiles/
else
    echo "MPICH already installed."
fi

#Make the .env file
cd "$ORIGINAL_DIR"/..
rm -f ./mpich.env

soft_name="mpich"
echo "Creating the $(pwd)/$soft_name.env file."
dir="$INSTALL_DIR/$soft_name"
[ -d "$dir/bin" ] && echo "export PATH=\"$dir/bin:\$PATH\"" >> $soft_name.env
[ -d "$dir/lib" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib:\$LD_LIBRARY_PATH\"" >> $soft_name.env
[ -d "$dir/lib32" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib32:\$LD_LIBRARY_PATH\"" >> $soft_name.env
[ -d "$dir/lib64" ] && echo "export LD_LIBRARY_PATH=\"$dir/lib64:\$LD_LIBRARY_PATH\"" >> $soft_name.env
pkg_config_dir=$(find "$dir" -name "pkgconfig")
if [ -n "$pkg_config_dir" ]; then
  echo "export PKG_CONFIG_PATH=\"$pkg_config_dir:\$PKG_CONFIG_PATH\"" >> $soft_name.env
fi
