#!/bin/env python

import sys
import os
import subprocess
import json

analysis_tools = ["pallas", "otf2", "pilgrim"]


MEM_MEASURE = ["/usr/bin/time", "-f", "mem_peak=%M"]

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Wrong usage: you need to provide one file that lists all the relevant 'analysis.log' files")
        exit(1)
    trace = sys.argv[1]
    if not os.path.isfile(trace):
        print("Experiment file does not exists.")
        exit(1)
    # This will be a list of path to traces
    trace_list = []

    # Different analysis to perform
    analysis_list = ["comm_matrix", "contention", "histogram"]
    # Which tool to run the analysis on
    tools = analysis_tools

    # Ensure rights on created files
    subprocess.Popen("umask 007", shell=True)
    all_traces_timings_lines = {"contention": {}, "comm_matrix": {}, "histogram": {}}

    outfile = { "contention": {}, "comm_matrix": {}, "histogram":{}}

    with open(sys.argv[1], "r") as listAnalysis:
        for analysisFileName in listAnalysis:
            analysisFileName = analysisFileName.replace("\n", "").replace("//", "/")
            if not os.path.isfile(analysisFileName):
                print(f"{analysisFileName} is not a file")
                continue
            with open(analysisFileName, "r") as analysisFile:
                trace_file = '/'.join(trace.split('/')[:-1])
                lines = analysisFile.readlines()
                lines = lines[1:]
                for line in lines:
                    exec_name, duration, mem_peak,size = line.split(',')
                    tool = "_".join(exec_name.split("_")[1:])
                    outfile[tool][trace_file] = f"{exec_name},{duration if duration else ''},{mem_peak if mem_peak else ''},{size}"

        with open("analysis.json", "w+") as of:
            json.dump(all_traces_timings_lines, of, indent=4)
        print (outfile)