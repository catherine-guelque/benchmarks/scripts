#!/bin/bash
ORIGINAL_DIR=$(pwd)

C_GREEN='\033[0;32m'
C_RED='\033[0;31m'
C_NC='\033[0m'

SUCCESS="${C_GREEN}Success${C_NC}"
FAILURE="${C_RED}Failure${C_NC}"

cd AMG
if make -j 8; then
  AMG_MESSAGE="AMG Compilation: [${SUCCESS}]"
else
  AMG_MESSAGE="AMG Compilation: [${FAILURE}]"
fi
cd "$ORIGINAL_DIR"

cd Kripke
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DENABLE_MPI=ON
if make -j 8; then
  Kripke_MESSAGE="Kripke Compilation: [${SUCCESS}]"
else
  Kripke_MESSAGE="Kripke Compilation: [${FAILURE}]"
fi
cd "$ORIGINAL_DIR"

cd LULESH
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
if make -j 8; then
  Lulesh_MESSAGE="Lulesh Compilation: [${SUCCESS}]"
else
  Lulesh_MESSAGE="Lulesh Compilation: [${FAILURE}]"
fi
cd "$ORIGINAL_DIR"

cd MiniFE/ref/src
if make -j 8; then
  MiniFE_MESSAGE="MiniFE Compilation: [${SUCCESS}]"
else
  MiniFE_MESSAGE="MiniFE Compilation: [${FAILURE}]"
fi
cd "$ORIGINAL_DIR"

cd NAS-Parallel-Benchmark/NPB3.4-MPI
if ./prepare.sh; then
  NAS_MESSAGE="NAS Compilation: [${SUCCESS}]"
else
  NAS_MESSAGE="NAS Compilation: [${FAILURE}]"
fi
cd "$ORIGINAL_DIR"

cd Quicksilver
cd src
if make -j; then
  Quicksilver_MESSAGE="Quicksilver Compilation: [${SUCCESS}]"
else
  Quicksilver_MESSAGE="Quicksilver Compilation: [${FAILURE}]"
fi
cd "$ORIGINAL_DIR"

echo -e "$AMG_MESSAGE"
echo -e "$Kripke_MESSAGE"
echo -e "$Lulesh_MESSAGE"
echo -e "$MiniFE_MESSAGE"
echo -e "$NAS_MESSAGE"
echo -e "$Quicksilver_MESSAGE"