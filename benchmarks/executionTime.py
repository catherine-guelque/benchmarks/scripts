#!/bin/env python
import json
import os
import re
import sys

import numpy as np

kernel_width = 13
size_width = 8
nMachines_width = 11
durations_width = 20


def amg_get_time(out: list[str]):
    lines = [line for line in out if "wall clock time" in line]
    if len(lines) > 0:
        clean_lines = [float(line.split(" = ")[-1].split(" ")[0]) for line in lines]
        return sum(clean_lines)
    return 0


def qs_get_time(out: list[str]):
    lines = [line for line in out if line[0:4] == "main"]
    if len(lines) > 0:
        return float(lines[0].split()[3]) / 1000
    return 0


def lulesh_get_time(out: list[str]):
    correct_lines = [l for l in out if "Elapsed time" in l]
    if len(correct_lines) > 0:
        return float(correct_lines[0].split("=")[-1].split("(s)")[0].strip())
    return 0


# This is actually not very precise, because Lulesh approximates at lot.

def kripke_get_time(out: list[str]):
    correct_lines = [line for line in out if "TIMER_DATA" in line]
    if len(correct_lines) > 0:
        correct_line = correct_lines[0]
        values = correct_line.split("TIMER_DATA:")[-1].split(",")
        return sum([float(v) for v in values])
    else:
        return 0


def nas_get_time(out: list[str]):
    lines = [line for line in out if "Time in seconds" in line]
    clean_lines = [float(line.split(" = ")[-1].strip()) for line in lines]
    if len(clean_lines) == 0:
        return 0
    return sum(clean_lines) / len(clean_lines)


def getDuration(kernel, filepath):
    with open(filepath, 'r') as f:
        if "nas" in kernel:
            return nas_get_time(f.readlines())
        if "kripke" in kernel:
            return kripke_get_time(f.readlines())
        if "amg" in kernel:
            return amg_get_time(f.readlines())
        if "lulesh" in kernel:
            return lulesh_get_time(f.readlines())
        if "quicksilver" in kernel:
            return qs_get_time(f.readlines())
        else:
            return 0


def addStats(directory, kernel, durations_dict):
    for log_file in [t for t in os.listdir(directory) if t.split(".")[-1] == "log"]:
        prefix = log_file.split(".log")[0]
        if prefix == "analysis" or prefix.count('_') != 3:
            continue
        pattern = r"^([^_]+)_([^_]+)_([^_]+)_([^_]+)$"
        match = re.match(pattern, prefix)
        parallel_tool = match.group(1)
        size = match.group(2)
        n_machines = int(match.group(3))
        n_iter = int(match.group(4))
        if size not in durations_dict:
            durations_dict[size] = {}
        if n_machines not in durations_dict[size]:
            durations_dict[size][n_machines] = []
        durations_dict[size][n_machines].append(
            getDuration(kernel, os.path.join(directory, f"{prefix}.log"))
        )


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Wrong usage: you need to provide one experiment file.")
        exit(1)
    experimentFile = sys.argv[1]
    if not os.path.isfile(experimentFile):
        print("Experiment file does not exists.")
        exit(1)
    kernel_tracingTool_size_nMachines_dict: dict[str, dict[str, dict[str, dict[int, list[int]]]]] = {}
    dataMode = False
    with open(experimentFile, "r") as f:
        for line in f:
            line = line.strip()
            if line == "# Experience name:":
                experienceName = f.readline()
                # print(experienceName)
                continue
            if line == "# Experience description:":
                experienceDescription = f.readline()
                # print(experienceDescription)
                continue
            if line == "# Data:":
                dataMode = True
                continue
            if line.strip() == "" or line[0] == '#':
                continue
            if dataMode:
                trace_directory = line
                pattern = r"results/([^/]+)/([^/]+)(/([^/]+))?/(?:[^/]+)$"
                match = re.search(pattern, trace_directory)
                tracing_tool = match.group(1)
                kernel = match.group(2)
                compression = match.group(4) if match.group(4) else None
                if tracing_tool == "pallas" or tracing_tool == "pilgrim":
                    tracing_tool = f"{tracing_tool}_{compression}"
                if kernel not in kernel_tracingTool_size_nMachines_dict:
                    kernel_tracingTool_size_nMachines_dict[kernel] = {}
                if tracing_tool not in kernel_tracingTool_size_nMachines_dict[kernel]:
                    kernel_tracingTool_size_nMachines_dict[kernel][tracing_tool] = {}
                addStats(trace_directory, kernel, kernel_tracingTool_size_nMachines_dict[kernel][tracing_tool])
    with open(f"{experimentFile.split('.')[0]}_timedata.json", "w") as outfile:
        json.dump(kernel_tracingTool_size_nMachines_dict, outfile, indent=2)
    print(
        "Kernel".ljust(kernel_width) +
        "Size".ljust(size_width) +
        "nMachines".ljust(nMachines_width),
        end=""
    )
    available_tracing_tools = []
    for kernel in kernel_tracingTool_size_nMachines_dict:
        for tool in kernel_tracingTool_size_nMachines_dict[kernel]:
            if tool not in available_tracing_tools:
                available_tracing_tools.append(tool)

    for tool in available_tracing_tools:
        print(tool.capitalize()[:durations_width].rjust(durations_width), end="")
    print()
    for kernel in kernel_tracingTool_size_nMachines_dict:
        available_size = []
        for tracingTool in kernel_tracingTool_size_nMachines_dict[kernel]:
            for size in kernel_tracingTool_size_nMachines_dict[kernel][tracingTool]:
                if size not in available_size:
                    available_size.append(size)
        for size in available_size:
            available_nMachines = []
            for tracingTool in kernel_tracingTool_size_nMachines_dict[kernel]:
                for size in kernel_tracingTool_size_nMachines_dict[kernel][tracingTool]:
                    for n_machines in kernel_tracingTool_size_nMachines_dict[kernel][tracingTool][size]:
                        if int(n_machines) not in available_nMachines:
                            available_nMachines.append(int(n_machines))
            available_nMachines.sort()
            for n_machines in available_nMachines:
                def try_and_get_value(tracing_tool):
                    try:
                        length = int(
                            np.mean(kernel_tracingTool_size_nMachines_dict[kernel][tracing_tool][size][n_machines]))
                        return f"""{length:,d}""".replace(',', ' ') if length else ""
                    except KeyError:
                        return ""

                print(f"{kernel}".ljust(kernel_width) +
                      f"{size}".ljust(size_width) +
                      f"{n_machines}".ljust(nMachines_width), end="")
                for tool in available_tracing_tools:
                    print(f"{try_and_get_value(tool)}".rjust(durations_width), end = "")
                print()
