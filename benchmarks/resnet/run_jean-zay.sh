#!/bin/bash

res_dir=/lustre/fsn1/projects/rech/avz/commun

date=$(date '+%Y-%m-%dT%H_%M_%S.%3N')
log_dir=/lustre/fsn1/projects/rech/avz/commun
export log_file="$log_dir/resnet_${date}.out"
export err_file="$log_dir/resnet_${date}.err"


# Clear the environment from any previously loaded modules
module purge > /dev/null 2>&1
#source ../../software/jeanzay.env
#module load pytorch-gpu/py3/2.3.0
module load emacs cmake llvm/15.0.6 libtool/2.4.6 automake/1.16.1 autoconf/2.69 hwloc/2.7.1 libpciaccess/0.16 libxml2/2.9.9 pytorch-gpu/py3/2.3.0

dataset_main_dir=/gpfsdswork/dataset/imagenet
train_dir=$dataset_main_dir/train
val_dir=$dataset_main_dir/val
options="--batch-size 128 --base-lr 0.1 --wd 1e-4"

function run_vanilla() {
    cmdline=$@
    echo "Running 'srun python $cmdline'"
    srun python $cmdline
}

function run_eztrace_otf2() {
    cmdline=$@

    source ../../software/otf2.env
    source ../../software/eztrace_pytorch_otf2.env

    export EZTRACE_NO_POSIXIO_EVENT=1
    export EZTPY_FUNCTION_FILTER=pytorch_functions.txt
    export PYTHON=$(which python)
    TRACE_DIRECTORY=$res_dir/resnet/trace_otf2_${date}

    which eztrace
    echo "Running: 'srun eztrace -o $TRACE_DIRECTORY -m -t "mpi python posixio" $cmdline'"
    srun eztrace -o $TRACE_DIRECTORY -m -t "mpi python posixio" $cmdline
}

function run_eztrace_pallas() {
    cmdline=$@

    source ../../software/pallas.env
    source ../../software/eztrace_pytorch_pallas.env

    export EZTRACE_NO_POSIXIO_EVENT=1
    export EZTPY_FUNCTION_FILTER=pytorch_functions.txt
    export PYTHON=$(which python)
    TRACE_DIRECTORY=$res_dir/resnet/trace_pallas_${date}

    which eztrace
    echo "Running: 'srun eztrace -o $TRACE_DIRECTORY -m -t "mpi python posixio" $cmdline'"
    srun eztrace -o $TRACE_DIRECTORY -m -t "mpi python posixio" $cmdline
}


run_type=vanilla
if [ -n "$1" ]; then
    run_type="$1"
fi

if [ "$run_type" = "eztrace_otf2" ]; then
    run_eztrace_otf2 pytorch_imagenet_resnet50.py --train-dir=$train_dir --val-dir=$val_dir --epochs=5 $options
elif [ "$run_type" = "eztrace_pallas" ]; then
    run_eztrace_pallas pytorch_imagenet_resnet50.py --train-dir=$train_dir --val-dir=$val_dir --epochs=5 $options
else
    run_vanilla pytorch_imagenet_resnet50.py --train-dir=$train_dir --val-dir=$val_dir --epochs=5 $options
fi

