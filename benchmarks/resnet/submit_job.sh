#!/bin/bash

SCRIPT_NAME=run_jean-zay.sh
ACCOUNT=avz@v100
NB_NODES=2
NB_GPU_PER_NODE=4

run_type=vanilla

if [ -n "$1" ]; then
    run_type="$1"
fi

sbatch --nodes=$NB_NODES --ntasks-per-node=$NB_GPU_PER_NODE --gres=gpu:$NB_GPU_PER_NODE -A $ACCOUNT $SCRIPT_NAME $run_type
 
