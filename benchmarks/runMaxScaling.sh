#!/bin/bash
source utils.sh

EXP_NAME="MaxScaling"
EXP_DESC="Run the kernels with only Pallas and Pilgrim, with ZSTD, SZ, with MAXIMUM SCALING."

echo "Saving this to ${res_dir}/${EXP_NAME}.md"
(
echo "# Experience name:"
echo "$EXP_NAME"
echo ""
echo "# Experience description:"
echo "$EXP_DESC"
echo ""
echo "# Data:"
) >> "${res_dir}/${EXP_NAME}.md"
DIRECTORY_SET=""


bench_list=(amg kripke lulesh minife quicksilver)

for bench in "${bench_list[@]}"; do
  COMPRESSION_LIST=(ZSTD SZ "")
  NTASKS_LIST=(512 4096)
  export n_iter=5
  run_"$bench"
done
echo "$DATA" | sort - >> "${res_dir}/${EXP_NAME}.md"
