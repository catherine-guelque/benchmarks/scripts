#!/bin/env python
# The goal of this script is to run the analysis for a different set of traces, corresponding to tracing applications
import math
import os
import sys
import json
import subprocess
from typing import List, Callable
from datetime import datetime
from datetime import date

BASEDIR = "/gpfswork/rech/avz/commun/scripts/"  # os.getcwd()
# PILGRIM_PATH = f"{BASEDIR}/software/install/pilgrim"
# EZTRACE_OTF2_PATH = f"{BASEDIR}/software/install/eztrace_otf2"
# EZTRACE_PALLAS_PATH = f"{BASEDIR}/software/install/eztrace_pallas"
PALLAS_PATH = f"{BASEDIR}/software/install/pallas"
OTF2_PATH = f"{BASEDIR}/software/install/otf2"
PILGRIM_PATH = f"{BASEDIR}/software/install/pilgrim"
PALLAS_COMM_MATRIX_PATH = f"{BASEDIR}/software/install/pallas-communication-matrix"
PALLAS_CONTENTION_PATH = f"{BASEDIR}/software/install/pallas-contention"
PALLAS_HISTOGRAM = f"{BASEDIR}/software/install/pallas-histogram"
OTF2_COMM_MATRIX_PATH = f"{BASEDIR}/software/install/otf2-communication-matrix"
OTF2_CONTENTION_PATH = f"{BASEDIR}/software/install/otf2-profile"
OTF2_HISTOGRAM = f"{BASEDIR}/software/install/otf2-histogram"
# LOG_PATH = f"{BASEDIR}/logs"
analysis_tools = ["pallas", "otf2", "pilgrim"]

# BASEDIR = os.getcwd()
DATE = datetime.now().strftime("%Y_%m_%d_%H_%M")

MEM_MEASURE = ["/usr/bin/time", "-f", "mem_peak=%M"]


def print_date_time(txt: str):
    print(f"[{date.today()} - {datetime.now().strftime('%H:%M')}] {txt}")


def get_elapsed_time(out: str):
    # Should be only one such line (time in nanoseconds)
    result = ""
    for res in out.split('\n'):
        if "Elapsed time" in res:
            result = res
    duration = result.split(":")[-1].split(" ")[1]
    return float(duration)


def get_max_memory_peak(result: str):
    # If errors else than time command output, get rid of them
    result = result[:-1]
    if "\n" in result:
        result = result.split('\n')[-1]
    return float(result.split('=')[1])


def get_dir_size(path: str):
    total = 0
    with os.scandir(path) as it:
        for entry in it:
            if entry.is_file():
                total += entry.stat().st_size
            elif entry.is_dir():
                total += get_dir_size(entry.path)
    return total


class Analysis:
    """Class to represent an analysis"""
    name: str
    exec_name: str
    # excluding trace name
    path_to_trace: str
    # name of the trace to input to the program
    trace_filename: str
    # command to start the analysis
    cmd: list
    # function to measure exec time
    get_execution_time: Callable[[str], float]
    # function to measure peak memory consumption
    get_peak_memory: Callable[[str], float]

    def __init__(self,
                 name: str,
                 exec_name: str,
                 path_to_trace: str,
                 trace_filename,
                 cmd: list,
                 get_execution_time: Callable[[str], float] = None,
                 get_peak_memory: Callable[[str], float] = None):
        self.name = name
        self.exec_name = exec_name
        self.path_to_trace = path_to_trace
        self.trace_filename = trace_filename
        self.cmd = cmd
        self.get_execution_time = get_execution_time
        self.get_peak_memory = get_peak_memory

    def run(self):
        current_dir = os.getcwd()

        print_date_time(f"Running {self.exec_name} for trace {self.path_to_trace}/{self.trace_filename}")
        os.chdir(self.path_to_trace)
        cmd_ = self.cmd
        cmd_ = MEM_MEASURE + cmd_
        print_date_time("> " + " ".join(cmd_))
        proc = subprocess.Popen(cmd_, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # proc.wait()
        try:
            out, err = proc.communicate(timeout=3600*.75)
        except subprocess.TimeoutExpired:
            duration, memorypeak = math.nan, math.nan
            print(f"Error running the analysis: Timeout")
            return -1, -1
        duration, memory_peak = None, None

        if proc.returncode != 0:
            print(f"Error running the analysis: {proc.returncode}")
            print_date_time(
                f"  Error : {proc.returncode} in {self.exec_name} for trace {self.path_to_trace}/{self.trace_filename}")
            print(err.decode("utf8"))
            return None
        else:
            if self.get_execution_time is not None:
                duration = self.get_execution_time(out.decode("utf8"))
            if self.get_peak_memory is not None:
                memory_peak = self.get_peak_memory(err.decode("utf8"))
        os.chdir(current_dir)
        print_date_time(f"Done running {self.exec_name} for trace {self.path_to_trace}/{self.trace_filename}...")
        return duration, memory_peak

    def print(self):
        print(self.name)
        print(self.exec_name)
        print(self.path_to_trace)
        print(self.trace_filename)
        print(self.get_execution_time)
        print(self.get_peak_memory)


class Pallas_analysis(Analysis):
    def __init__(self,
                 name: str,
                 exec_name: str,
                 path_to_trace: str,
                 cmd: List[str] = None):
        super().__init__(name=name,
                         exec_name=exec_name,
                         path_to_trace=path_to_trace,
                         trace_filename="eztrace_log.pallas",
                         cmd=cmd,
                         get_execution_time=get_elapsed_time,
                         get_peak_memory=get_max_memory_peak)
        # Command to run Pallas analysis: comm_matrix and contention
        if name == "histogram":
            self.cmd = [self.exec_name, self.trace_filename]
        else:
            self.cmd = [self.exec_name, "-t", self.trace_filename]


class OTF2_analysis(Analysis):
    def __init__(self,
                 name: str,
                 exec_name: str,
                 path_to_trace: str,
                 cmd: List[str] = None):
        super().__init__(name=name,
                         exec_name=exec_name,
                         path_to_trace=path_to_trace,
                         trace_filename="eztrace_log.otf2",
                         cmd=cmd,
                         get_execution_time=get_elapsed_time,
                         get_peak_memory=get_max_memory_peak)
        if name == "contention":
            # Add option in commande line for otf2_contention (old oft2_profile) to get full verbose
            self.cmd = [self.exec_name, self.trace_filename]
            self.get_execution_time = None
        elif name == "comm_matrix" or name == "histogram":
            self.cmd = [self.exec_name, self.trace_filename]


class Pilgrim_analysis(Analysis):
    def __init__(self,
                 name: str,
                 exec_name: str,
                 path_to_trace: str,
                 cmd: List[str] = None):
        super().__init__(name=name,
                         exec_name=exec_name,
                         path_to_trace=path_to_trace,
                         trace_filename="",
                         cmd=cmd,
                         get_execution_time=get_elapsed_time,
                         get_peak_memory=get_max_memory_peak)
        self.cmd = [self.exec_name, self.path_to_trace]


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Wrong usage: you need to provide one experiment file.")
        exit(1)
    trace = sys.argv[1]
    if not os.path.isdir(trace):
        print("Experiment file does not exists.")
        exit(1)
    # This will be a list of path to traces
    trace_list = []

    # TODO : make it better
    path_to_traces = "/lustre/fsn1/projects/rech/avz/commun"
    analysis_file_name = "analysis.log"

    # Different analysis to perform
    analysis_list = ["comm_matrix", "contention", "histogram"]
    # Which tool to run the analysis on
    tools = analysis_tools

    os.environ["PATH"] = (f"{PALLAS_COMM_MATRIX_PATH}/bin:"
                          f"{PALLAS_PATH}/bin:"
                          f"{PALLAS_CONTENTION_PATH}/bin:"
                          f"{PALLAS_HISTOGRAM}/bin:"
                          f"{OTF2_COMM_MATRIX_PATH}/bin:"
                          f"{OTF2_CONTENTION_PATH}/bin:"
                          f"{OTF2_HISTOGRAM}/bin:"
                          f"{OTF2_PATH}/bin:"
                          f"{PILGRIM_PATH}/bin:"
                          f"{os.environ['PATH']}")
    os.environ["LD_LIBRARY_PATH"] = (f"{PALLAS_PATH}/lib64:"
                                     f"{OTF2_PATH}/lib:"
                                     f"{os.environ['LD_LIBRARY_PATH']}")

    # Ensure rights on created files
    subprocess.Popen("umask 007", shell=True)
    all_traces_timings_lines = {"contention": {}, "comm_matrix": {}}

    # For all tools
    tool: str
    for tool in tools:
        if tool in trace:
            break
    else:
        print(f"WARNING: Trace found with unknown tool: {trace}", file=sys.stderr)
        exit(1)
    # Check trace validity
    if tool == "pallas" and not os.path.isfile(f"{trace}/eztrace_log.pallas"):
        print(f"WARNING : Main trace file does not exists: {trace}/eztrace_log.pallas", file=sys.stderr)
        exit(1)
    if tool == "otf2" and not os.path.isfile(f"{trace}/eztrace_log.otf2"):
        print(f"WARNING : Main trace file does not exists: {trace}/eztrace_log.otf2", file=sys.stderr)
        exit(1)
    if tool == "pilgrim" and not os.path.isfile(f"{trace}/pilgrim.mt"):
        print(f"WARNING : Main trace file does not exists: {trace}/pilgrim.mt", file=sys.stderr)
        exit(1)

    # Timestamp for log file
    outputFile = f"{trace}/{analysis_file_name}"
    outputLines = [f"{date.today()} - {datetime.now().strftime('%H:%M')}"]

    # For each analysis
    for analysis in analysis_list:
        if tool == "pilgrim" and analysis != "comm_matrix":
            continue
        results = None
        exec_name = f"{tool}_{analysis}"

        # Treat if Pallas
        if tool == "pallas":
            results = Pallas_analysis(analysis, exec_name, trace).run()
        elif tool == "otf2":
            results = OTF2_analysis(analysis, exec_name, trace).run()
        elif tool == "pilgrim":
            results = Pilgrim_analysis(analysis, "pilgrimCommMatrix", trace).run()

        if results is not None:
            # Run the analysis
            duration, mem_peak = results
            # Get durations in seconds
            duration = duration / 1e9 if duration else None
            # Get mem peak in GB
            mem_peak = mem_peak / 1e6 if mem_peak else None
            # Get size in GB
            size = float(get_dir_size(trace)) / 1e9

            print(f"Analysis = {exec_name} - "
                  f"Duration = {duration} sec - "
                  f"Mem peak = {mem_peak} GB - "
                  f"Trace size = {size} GB - "
                  f"Trace path = {trace}")
            line = f"\n{exec_name},{duration if duration else ''},{mem_peak if mem_peak else ''},{size}"
            outputLines.append(line)
            all_traces_timings_lines[analysis][trace] = line
        else:
            print(f"WARNING: Results were none, check stderr for clues", file=sys.stderr)

    if len(outputLines) > 1:
        with open(outputFile, "w") as f:
            f.writelines(outputLines)
