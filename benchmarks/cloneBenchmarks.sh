#!/bin/bash
#URI="ssh://git@gitlab.inf.telecom-sudparis.eu:2222/catherine-guelque/benchmarks"
URI="http://gitlab.inf.telecom-sudparis.eu/catherine-guelque/benchmarks"
git clone $URI/AMG.git

git clone $URI/Kripke.git
cd Kripke
git submodule update --init --recursive
cd ..

git clone $URI/LULESH.git

git clone $URI/MiniFE.git

git clone $URI/NAS-Parallel-Benchmark.git

git clone $URI/Quicksilver.git
