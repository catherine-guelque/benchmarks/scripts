#!/bin/bash
if [ "$#" -ne 1 ]; then
  echo "Need one file to read from."
  exit 1
fi

if ! [ -f "$1" ]; then
  echo "Provided file does not exists: $1"
  exit 2
fi

umask 007
date=$(date '+%Y-%m-%dT%H_%M_%S.%3N')
account=avz@cpu

export output_prefix="pallas_analysis"
export run_time="02:30:00"

exec 3<"$1"

while [ "$line" != "# Data:" ]; do
  read line <&3
done
i=1
while read line <&3; do
  [ -z "$line" ] && continue
  if echo "$line" | grep -v -E "otf2|pallas|pilgrim" >/dev/null; then
    continue
  fi
  for trace in "$line"/mpi_*_trace/; do
    echo "$trace"
    echo "#!/bin/bash
source /lustre/fswork/projects/rech/avz/commun/scripts/software/jeanzay.env
source /lustre/fswork/projects/rech/avz/commun/scripts/software/pallas.env
set -x
srun python3 analysisBenchmark.py $trace"   |
    sbatch  -A "$account"           \
        --job-name=pallas_analysis \
        --output="${output_prefix}_${i}.log"        \
        --error="${output_prefix}_${i}.err"         \
        --ntasks=1                  \
        --cpus-per-task=40          \
        --hint=nomultithread        \
        --time=${run_time}
    i=$(expr $i + 1)
    done
done

# 40 CPUs for 156Gb of memory
# http://www.idris.fr/eng/jean-zay/cpu/jean-zay-cpu-exec_alloc-mem-eng.html