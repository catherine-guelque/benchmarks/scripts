#!/bin/bash
source utils.sh
if ! [ $# -eq 2 ]; then
    echo "Need exactly two argument."
    exit 1
fi

export nas_name="$1"
app_name="nas_${nas_name}"
cd NAS-Parallel-Benchmark
nas_dir="NPB3.4-MPI/bin"

if ! ls "$nas_dir/$nas_name."[A-E]".x" 1> /dev/null 2>&1; then
    echo "Given NAS did not exist. Please choose from the available NAS:"
    ls "$nas_dir/" | cut -d '.' -f 1 | uniq | tr '\n' ' '
    echo ""
    exit 2
fi

process_constraint_grid="lu"
process_constraint_power_two="cg ft is mg"
process_constraint_squared="bt sp"
process_constraint_none="ep"

function exists_in_list() {
    for word in $2; do
	if [ "$word" = "$1" ]; then
	    return 0;
	fi
    done
    return 1;
}

if [ "$2" = "CompareCompression" ]; then
  n_tasks_list="10 40 80"
  if exists_in_list $nas_name "$process_constraint_grid"; then
      n_tasks_list="16 32 64 128"
  fi
  if exists_in_list $nas_name "$process_constraint_power_two"; then
      n_tasks_list="16 32 64 128"
  fi
  if exists_in_list $nas_name "$process_constraint_squared"; then
      n_tasks_list="16 36 64 169"
  fi
  export pb_size="C"
  export pb_size_name="big"
  export n_iter=5
  run_time="01:00:00"
elif [ "$2" = "MaxScaling" ]; then
  n_tasks_list="512 4096"
  if exists_in_list $nas_name "$process_constraint_grid"; then
      n_tasks_list="512 4096"
  fi
  if exists_in_list $nas_name "$process_constraint_power_two"; then
      n_tasks_list="512 4096"
  fi
  if exists_in_list $nas_name "$process_constraint_squared"; then
      n_tasks_list="256 4096"
  fi

  export pb_size="D"
  export pb_size_name="bigger"

  export n_iter=1
  run_time="01:00:00"
fi

    for ntasks in $n_tasks_list ; do
      export ntasks
      ! [ "$IGNORE_VANILLA" = "1" ] && run_vanilla
      ! [ "$IGNORE_EZTRACE" = "1" ] && run_eztrace
      ! [ "$IGNORE_PALLAS" = "1" ] && run_pallas
      ! [ "$IGNORE_PILGRIM" = "1" ] && run_pilgrim
    done
cd ..
