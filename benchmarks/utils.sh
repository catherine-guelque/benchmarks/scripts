#!/bin/bash
umask 007
date=$(date '+%Y-%m-%dT%H_%M_%S.%3N')
res_dir=/lustre/fsn1/projects/rech/avz/commun
account=avz@cpu
DIRNAME=$(realpath "$(dirname "$0")")

function run_tests() {
  for NTASKS in "${NTASKS_LIST[@]}"; do
    [ -z "$NTHREADS" ] && NTHREADS=1
    echo "Running ${APP_NAME} for ${NTASKS}x${NTHREADS}: $args"
    for COMPRESSION in "${COMPRESSION_LIST[@]}"; do
      var_name="${APP_NAME}_args_${NTASKS}x${NTHREADS}"
      ARGS=${!var_name}
      RUN_TIME=$(date -d@"$(expr ${n_iter} \* "${APP_NAME}_weak_runtime")" -u +%H:%M:%S)
      ! [ "$IGNORE_VANILLA" = "1" ] && run_vanilla
      ! [ "$IGNORE_EZTRACE" = "1" ] && run_eztrace
      ! [ "$IGNORE_PALLAS" = "1" ] && run_pallas
      ! [ "$IGNORE_PILGRIM" = "1" ] && run_pilgrim
    done
  done
}


amg_args_512x1=" -P 8  8  8 -n 96 96 96"
amg_args_4096x1="-P 16 16 16 -n 96 96 96"
amg_weak_runtime="1800"
function run_amg() {
  cd AMG
  APP_NAME="amg"
  run_tests
  cd - >/dev/null
}

kripke_args_512x1="--procs 8,8,8 --zones 64,64,64"
kripke_args_4096x1="--procs 16,16,16 --zones 128,128,128"
kripke_weak_runtime="600"
function run_kripke() {
  cd Kripke
  APP_NAME="kripke"
  run_tests
  cd - >/dev/null
}

lulesh_args_512x1="-s 100 -i 10"
lulesh_args_4096x1="-s 100 -i 10"
lulesh_weak_runtime="1200"
function run_lulesh() {
  cd LULESH
  APP_NAME="lulesh"
  run_tests
  cd - >/dev/null
}

# TODO Minife 4096 isn't correct
# TODO Correct the runtime
minife_args_512x1="-nx 512 ‐ny 512 -nz 512"
minife_args_4096x1="-nx 4096 ‐ny 4096 -nz 4096"
minife_weak_runtime="1200"
function run_minife() {
  cd MiniFE
  APP_NAME="minife"
  run_tests
  cd - >/dev/null
}

qs_input_path="${DIRNAME}/Quicksilver/Examples/CORAL2_Benchmark/Problem1/Coral2_P1.inp"
quicksilver_args_512x1="-i ${qs_input_path} -X 64  -Y 64  -Z 32  -x 64  -y 64  -z 32  -I 8  -J 8  -K 8  -n 5242880"
quicksilver_args_4096x1="-i ${qs_input_path} -X 128 -Y 128 -Z 64  -x 128 -y 128 -z 64  -I 16 -J 16 -K 16 -n 41943040"
quicksilver_weak_runtime="3200"
function run_quicksilver() {
  cd Quicksilver
  APP_NAME="quicksilver"
  run_tests
  cd - >/dev/null
}



function create_output_directory() {
  tracer="$1"
  compression="$2"
  if [ -z "$compression" ]; then
    output_dir="$res_dir/${tracer}/${APP_NAME}/${date}"
  else
    output_dir="$res_dir/${tracer}/${compression}/${APP_NAME}/${date}"
  fi
  mkdir -p "$output_dir"
  if ! echo "${DIRECTORY_SET}" | grep "$output_dir" > /dev/null; then
      DIRECTORY_SET="${DIRECTORY_SET}${output_dir}"$'\n'
  fi
  echo "$output_dir"
}

# Each function called run_ must be called with the following parameters:
#   - APP_NAME: Name of the app
#   - NTASKS: Number of MPI Processes
#   - NTHREADS: Number of OMP Threads
#   - RUN_TIME: Max runtime of the app
#   - n_iter: must be exported, number of iterations

function run_eztrace() {
    [ ! -z "$COMPRESSION" ] && return 0
    echo "    Running ${APP_NAME} with EZTrace/OTF2 on ${NTASKS}x${NTHREADS}"
    output_dir=$(create_output_directory otf2)
    export output_dir

    log_file="${output_dir}/mpi_${NTASKS}x${NTHREADS}.log"
    err_file="${output_dir}/mpi_${NTASKS}x${NTHREADS}.err"
    export OMP_NUM_THREADS=${NTHREADS}
    echo "        log file: $log_file"
    [ "$DRY_RUN" = 1 ] && return 0

    sbatch -A "$account"\
	   --job-name=run_"${APP_NAME}"_cpu_eztrace\
	   --output="$log_file" \
	   --error="$err_file" \
	   --ntasks="$NTASKS"   \
	   --cpus-per-task="$NTHREADS"   \
	   --hint=nomultithread  \
	   --time=${RUN_TIME}       \
	   run_eztrace.slurm
}

function run_pallas() {
    echo "    Running $APP_NAME with EZTrace/Pallas on ${NTASKS}x${NTHREADS}"
    if [ -z "$COMPRESSION" ]; then
      local COMPRESSION="NO_COMP"
      export PALLAS_COMPRESSION="None"
    else
      echo "        Pallas compression: using ${COMPRESSION}"
      export PALLAS_COMPRESSION="$COMPRESSION"
    fi
    if ! [ -z "$ENCODING" ]; then
      echo "        Pallas encoding: using ${ENCODING}"
      export PALLAS_ENCODING="$ENCODING"
      local ENCODING="_${ENCODING}"
    fi
    output_dir=$(create_output_directory pallas "$COMPRESSION$ENCODING")
    export output_dir
    export OMP_NUM_THREADS=$NTHREADS
    log_file="${output_dir}/mpi_${NTASKS}x${NTHREADS}.log"
    err_file="${output_dir}/mpi_${NTASKS}x${NTHREADS}.err"
    echo "        log file: $log_file"

    [ "$DRY_RUN" = 1 ] && return 0

    sbatch -A "$account"\
	   --job-name=run_"${APP_NAME}"_cpu_pallas\
	   --output="$log_file" \
	   --error="$err_file" \
	   --ntasks="$NTASKS"   \
	   --cpus-per-task="$NTHREADS"   \
	   --hint=nomultithread  \
	   --time=${RUN_TIME}       \
	   run_pallas.slurm
}

function run_pilgrim() {
    echo "    Running $APP_NAME with Pilgrim on ${NTASKS}x${NTHREADS}"
    if ! [ "$COMPRESSION" = "" ]; then
      echo "        Pilgrim compression: using ${COMPRESSION}"
      export PILGRIM_TIMING_MODE="$COMPRESSION"
    else
      local COMPRESSION="NO_COMP"
      export PILGRIM_TIMING_MODE="LOSSLESS"
    fi
    output_dir=$(create_output_directory PILGRIM "$COMPRESSION")
    export output_dir
    export OMP_NUM_THREADS=$NTHREADS
    log_file="${output_dir}/mpi_${NTASKS}x${NTHREADS}.log"
    err_file="${output_dir}/mpi_${NTASKS}x${NTHREADS}.err"
    echo "        log file: $log_file"

    [ "$DRY_RUN" = 1 ] && return 0

    sbatch -A "$account"\
	   --job-name=run_"${APP_NAME}"_cpu_pilgrim\
	   --output="$log_file" \
	   --error="$err_file" \
	   --ntasks="$NTASKS"   \
	   --cpus-per-task="$NTHREADS"   \
	   --hint=nomultithread  \
	   --time=${RUN_TIME}       \
	   run_pilgrim.slurm
}

function run_vanilla() {
    [ ! -z "$COMPRESSION" ] && return 0
    echo "    Running $APP_NAME on ${NTASKS}x${NTHREADS}"
    output_dir=$(create_output_directory vanilla)
    export output_dir
    export OMP_NUM_THREADS=$NTHREADS
    log_file="${output_dir}/mpi_${NTASKS}x${NTHREADS}.log"
    err_file="${output_dir}/mpi_${NTASKS}x${NTHREADS}.err"
    echo "        log file: $log_file"

    [ "$DRY_RUN" = 1 ] && return 0

    sbatch -A "$account"\
	   --job-name=run_"${APP_NAME}"_cpu_vanilla\
	   --output="$log_file" \
	   --error="$err_file" \
	   --ntasks="$NTASKS"   \
	   --cpus-per-task="$NTHREADS"   \
	   --hint=nomultithread  \
	   --time=${RUN_TIME}       \
	   run_vanilla.slurm
}
