#!/bin/bash
cd AMG
make veryclean
cd ..

cd Kripke
rm -rf build
cd ..

cd LULESH
rm -rf build
cd ..

cd NAS-Parallel-Benchmark/NPB3.4-MPI
make clean
rm -rf bin
cd ../..

cd Quicksilver/src
make clean
cd ../..
