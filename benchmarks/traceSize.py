#!/bin/env python
import os
import re
import sys

import numpy as np
import json


def get_dir_size(path):
    total = 0
    with os.scandir(path) as it:
        for entry in it:
            if entry.is_file():
                total += entry.stat().st_size
            elif entry.is_dir():
                total += get_dir_size(entry.path)
    return total


kernel_width = 13
size_width = 8
nMachines_width = 11
trace_size_width = 20


def addStats(directory, trace_size_dict):
    for trace in [t for t in os.listdir(directory) if os.path.isdir(os.path.join(directory, t))]:
        prefix = trace.split("_trace")[0]
        pattern = r"^([^_]+)_([^_]+)_([^_]+)_([^_]+)$"
        match = re.match(pattern, prefix)
        parallel_tool = match.group(1)
        size = match.group(2)
        n_machines = int(match.group(3))
        n_iter = int(match.group(4))
        if size not in trace_size_dict:
            trace_size_dict[size] = {}
        if n_machines not in trace_size_dict[size]:
            trace_size_dict[size][n_machines] = []
        trace_size_dict[size][n_machines].append(get_dir_size(os.path.join(directory, trace)))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Wrong usage: you need to provide one experiment file.")
        exit(1)
    experimentFile = sys.argv[1]
    if not os.path.isfile(experimentFile):
        print("Experiment file does not exists.")
        exit(1)
    kernel_tracingTool_size_nMachines_dict: dict[str, dict[str, dict[str, dict[int, list[int]]]]] = {}
    dataMode = False
    with open(experimentFile, "r") as f:
        for line in f:
            line = line.strip()
            if line == "# Experience name:":
                experienceName = f.readline()
                # print(experienceName)
                continue
            if line == "# Experience description:":
                experienceDescription = f.readline()
                # print(experienceDescription)
                continue
            if line == "# Data:":
                dataMode = True
                continue
            if line.strip() == "" or line[0] == '#':
                continue
            if dataMode:
                trace_directory = line
                pattern = r"results/([^/]+)/([^/]+)(/([^/]+))?/(?:[^/]+)$"
                match = re.search(pattern, trace_directory)
                tracing_tool = match.group(1)
                kernel = match.group(2)
                compression = match.group(4) if match.group(4) else None
                if tracing_tool == "pallas" or tracing_tool == "pilgrim":
                    tracing_tool = f"{tracing_tool}_{compression}"
                if kernel not in kernel_tracingTool_size_nMachines_dict:
                    kernel_tracingTool_size_nMachines_dict[kernel] = {}
                if tracing_tool not in kernel_tracingTool_size_nMachines_dict[kernel]:
                    kernel_tracingTool_size_nMachines_dict[kernel][tracing_tool] = {}
                addStats(trace_directory, kernel_tracingTool_size_nMachines_dict[kernel][tracing_tool])
    with open(f"{experimentFile.split('.')[0]}_sizedata.json", "w") as outfile:
        json.dump(kernel_tracingTool_size_nMachines_dict, outfile, indent=2)
    print("All shown sizes are in Kib")
    print(
        "Kernel".ljust(kernel_width) +
        "Size".ljust(size_width) +
        "nMachines".ljust(nMachines_width)
        , end="")

    available_tracing_tools = []
    for kernel in kernel_tracingTool_size_nMachines_dict:
        for tool in kernel_tracingTool_size_nMachines_dict[kernel]:
            if tool not in available_tracing_tools:
                available_tracing_tools.append(tool)

    for tool in available_tracing_tools:
        print(tool.capitalize()[:trace_size_width].rjust(trace_size_width), end="")
    print()
    for kernel in kernel_tracingTool_size_nMachines_dict:
        available_size = []
        for tracingTool in kernel_tracingTool_size_nMachines_dict[kernel]:
            for size in kernel_tracingTool_size_nMachines_dict[kernel][tracingTool]:
                if size not in available_size:
                    available_size.append(size)
        for size in available_size:
            available_nMachines = []
            for tracingTool in kernel_tracingTool_size_nMachines_dict[kernel]:
                for size in kernel_tracingTool_size_nMachines_dict[kernel][tracingTool]:
                    for n_machines in kernel_tracingTool_size_nMachines_dict[kernel][tracingTool][size]:
                        if n_machines not in available_nMachines:
                            available_nMachines.append(n_machines)
            available_nMachines.sort()
            for n_machines in available_nMachines:
                def try_and_get_value(tracing_tool):
                    try:
                        return f"""{int(
                            np.mean(kernel_tracingTool_size_nMachines_dict[kernel][tracing_tool][size][n_machines])
                            / 1024):,d}""".replace(',', ' ')
                    except KeyError:
                        return ""

                print(f"{kernel}".ljust(kernel_width) +
                      f"{size}".ljust(size_width) +
                      f"{n_machines}".ljust(nMachines_width), end="")
                for tool in available_tracing_tools:
                    print(f"{try_and_get_value(tool)}".rjust(trace_size_width), end = "")
                print()
