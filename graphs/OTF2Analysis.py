#!/bin/python
# Imports
import json
import re
import sys
from operator import attrgetter
from typing import List

import matplotlib.pyplot as plt
import os

from matplotlib.patches import Patch, FancyBboxPatch
from matplotlib.patches import Rectangle

import numpy as np

colors = {
    "OTF2": "tab:blue",
    "Pallas": "tab:orange",
    "Pilgrim": "tab:green"
}


def getColor(attribute: str):
    for tool in colors.keys():
        if tool.upper() == attribute.upper():
            return colors[tool]


rewriteKeys = {
    "pallas": "Pallas",
    "otf2": "OTF2",
    "pilgrim": "Pilgrim"
}


def plotDuration(data: dict, title: str, name: str):
    kernel_nProc_data = {}
    for fullPath in data:
        if not "otf2" in fullPath:
            continue
        analysisData = data[fullPath].split(',')
        fullPath = fullPath.split('/')
        traceName = fullPath[-1]
        nProc = int(traceName.split('_')[-3])
        kernel: str = fullPath[-3]
        if "nas" in kernel:
            kernel = kernel.replace('_', ' ').upper()
        else:
            kernel = kernel.capitalize()
        if not kernel in kernel_nProc_data:
            kernel_nProc_data[kernel] = {}
        if not nProc in kernel_nProc_data[kernel]:
            kernel_nProc_data[kernel][nProc] = []
        kernel_nProc_data[kernel][nProc].append(float(analysisData[1]))

    fig, ax = plt.subplots(figsize=(12, 8))
    categories = list(kernel_nProc_data.keys())
    categories.sort()
    n_categories = len(categories)
    categories_x_list = np.arange(n_categories)
    column_width = 0.6
    std_factor = 1
    # Interval de confiance à 99%

    for kernel in kernel_nProc_data:
        nProc = max(kernel_nProc_data[kernel].keys())
        mean = np.mean(kernel_nProc_data[kernel][nProc])
        catIndex = categories.index(kernel)
        ax.bar(
            [categories_x_list[catIndex]],
            [mean],
            yerr=std_factor * np.std(kernel_nProc_data[kernel][nProc]),
            capsize=20 * column_width,
            width=column_width,
            color=getColor(rewriteKeys["otf2"])
        )
    ax.set_xticks(categories_x_list)
    categories = [f"{c} {max(kernel_nProc_data[c].keys())}" for c in categories]
    ax.set_xticklabels(categories, rotation=30, ha="right")
    ax.yaxis.grid(True)
    ax.set_axisbelow(True)
    plt.tight_layout(rect=(0.01, 0.01, 1, .89))

    ax.set_ylabel("Execution time (s)")
    fig.suptitle(title)
    fig.savefig(name, dpi=300)


def plotMemoryPercent(data: dict, title: str, name: str):
    kernel_nProc_data = {}
    for fullPath in data:
        if not "otf2" in fullPath:
            continue
        analysisData = data[fullPath].split(',')
        fullPath = fullPath.split('/')
        traceName = fullPath[-1]
        nProc = int(traceName.split('_')[-3])
        kernel: str = fullPath[-3]
        if kernel in ["nas_ep", "nas_ft", "nas_is"]:
            continue
        if "nas" in kernel:
            kernel = kernel.replace('_', ' ').upper()
        else:
            kernel = kernel.capitalize()
        if not kernel in kernel_nProc_data:
            kernel_nProc_data[kernel] = {}
        if not nProc in kernel_nProc_data[kernel]:
            kernel_nProc_data[kernel][nProc] = []
        kernel_nProc_data[kernel][nProc].append(float(analysisData[2]) / float(analysisData[3]))

    fig, ax = plt.subplots(figsize=(12, 8))
    categories = list(kernel_nProc_data.keys())
    categories.sort()
    n_categories = len(categories)
    categories_x_list = np.arange(n_categories)
    column_width = 0.6
    std_factor = 1
    # Interval de confiance à 99%

    for kernel in kernel_nProc_data:
        nProc = max(kernel_nProc_data[kernel].keys())
        mean = np.mean(kernel_nProc_data[kernel][nProc])
        catIndex = categories.index(kernel)
        ax.bar(
            [categories_x_list[catIndex]],
            [mean],
            yerr=std_factor * np.std(kernel_nProc_data[kernel][nProc]),
            capsize=20 * column_width,
            width=column_width,
            color=getColor(rewriteKeys["otf2"])
        )
    ax.set_xticks(categories_x_list)
    categories = [f"{c} {max(kernel_nProc_data[c].keys())}" for c in categories]
    ax.set_xticklabels(categories, rotation=30, ha="right")
    ax.yaxis.grid(True)
    ax.set_axisbelow(True)
    plt.tight_layout(rect=(0.01, 0.01, 1, .89))

    ax.set_ylabel("Memory usage (% of trace size)")
    fig.suptitle(title)
    fig.savefig(name, dpi=300)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: OTF2Analysis <ExperimentName>")
        print("With <ExperimentName>_analysisdata.json is a file that exists.")
        exit(1)
    experimentName = sys.argv[1]
    comMatrixData: dict
    contentionData: dict
    with open(f"{experimentName}_analysisdata.json", "r") as infile:
        data = json.load(infile)
        comMatrixData = data["comm_matrix"]
        contentionData = data["contention"]
    plt.rcParams.update({'font.size': 30})
    plt.rcParams.update({'mathtext.fontset': 'stix'})
    plt.rcParams.update({'font.family': 'STIXGeneral'})
    figHeight, figwWidth = 10, 20
    fig, ax = plt.subplots(figsize=(figwWidth, figHeight))
    plotDuration(comMatrixData,
                           f"Time to plot a communication matrix\n from an OTF2 Trace.",
                           f"OTF2 All Kernel Matrix Time.svg"
                 )
    plotMemoryPercent(comMatrixData,
                                f"Memory consumption to plot a communication matrix\nfrom an OTF2 Trace.",
                                "OTF2 All Kernel Matrix Memory.svg")
    plotMemoryPercent(contentionData,
                      f"Memory consumption to detect contention\nfrom an OTF2 Trace.",
                      "OTF2 All Kernel Contention Memory.svg")
    # plotContention(contentionData)
