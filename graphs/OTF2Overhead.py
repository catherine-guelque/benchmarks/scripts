#!/bin/python
# Imports
import json
import re
import sys
from operator import attrgetter

import matplotlib.pyplot as plt
import os

from matplotlib.patches import Patch, FancyBboxPatch
from matplotlib.patches import Rectangle

import numpy as np
from matplotlib.ticker import FormatStrFormatter

colors = {
    "OTF2": "tab:blue",
    "Pallas": "tab:orange",
    "Pilgrim": "tab:green"
}


def getColor(attribute: str):
    if attribute == "vanilla":
        return colors["No Tracing"]
    for tool in colors.keys():
        if tool.upper() == attribute.upper():
            return colors[tool]


rewriteKeys = {
    "vanilla": "No tracing",
    "pallas_NO_COMP": "Pallas Vanilla",
    "pallas_ZSTD": "Pallas ZSTD",
    "pallas_ZFP": "Pallas ZFP",
    "otf2": "OTF2"
}

kernel_tracingTool_size_nMachines_dict: dict[str, dict[str, dict[str, dict[str, list[int]]]]] = {}

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: CompareCompression <ExperimentName>")
        print("With <ExperimentName>_timedata.json is a file that exists.")
        exit(1)
    experimentName = sys.argv[1]
    with open(f"{experimentName}_timedata.json", "r") as infile:
        kernel_tracingTool_size_nMachines_dict = json.load(infile)
    plt.rcParams.update({'font.size': 30})
    plt.rcParams.update({'mathtext.fontset': 'stix'})
    plt.rcParams.update({'font.family': 'STIXGeneral'})
    figHeight, figwWidth = 10, 20
    fig, ax = plt.subplots(figsize=(figwWidth, figHeight))
    blacklist = []
    list_kernel = [k for k in kernel_tracingTool_size_nMachines_dict.keys() if k not in blacklist]
    n_categories = len(list_kernel)
    categories_x_list = np.arange(n_categories)
    subcategories_x_list_list = []
    tick_labels = []
    for kernel in list_kernel:
        print(f"Plotting kernel: {kernel}")
        tracingTool_size_nMachines_dict = kernel_tracingTool_size_nMachines_dict[kernel]
        # availableTracingTools = list(tracingTool_size_nMachines_dict.keys())
        availableTracingTools = ['otf2']
        n_tracing_tool = len(availableTracingTools)

        available_size = []
        for tracingTool in availableTracingTools:
            for size in tracingTool_size_nMachines_dict[tracingTool]:
                if size not in available_size:
                    available_size.append(size)
        size = available_size[-1]
        del available_size
        available_nMachines = []
        for tracingTool in availableTracingTools:
            for nMachine in tracingTool_size_nMachines_dict[tracingTool][size]:
                if int(nMachine) not in available_nMachines:
                    available_nMachines.append(int(nMachine))
        nMachine = max(available_nMachines)
        del available_nMachines

        n_subcategories = len(availableTracingTools)
        subcategories_x_list = np.linspace(0, 1, num=n_subcategories + 2)
        subcategories_x_list_list.append(subcategories_x_list)
        column_width = 1 / (n_subcategories + 1)
        std_factor = 1
        tick_labels.append(f"{kernel.upper().replace('_', ' ')}" if "nas" in kernel else kernel.capitalize())
        tick_labels[-1] += f" {nMachine}"
        # Interval de confiance à 99%
        vanilla_timings_mean = np.mean(
            [t for t in tracingTool_size_nMachines_dict["vanilla"][size][str(nMachine)] if t])
        for tracingTool in availableTracingTools:
            raw_tracingTool = tracingTool.split("_")[0]
            trace_size = np.array([t for t in tracingTool_size_nMachines_dict[tracingTool][size][str(nMachine)] if t])
            catIndex = list_kernel.index(kernel)
            subcatIndex = availableTracingTools.index(tracingTool)
            mean = np.mean(trace_size) / vanilla_timings_mean
            ax.bar(
                [categories_x_list[catIndex] + subcategories_x_list[subcatIndex]],
                [mean],
                yerr=std_factor * np.std(trace_size) / vanilla_timings_mean,
                capsize=40 * column_width,
                width=column_width,
                label=raw_tracingTool,
                color=getColor(raw_tracingTool)
            )
    ax.set_xticks(categories_x_list + [np.mean(s[:-1]) for s in subcategories_x_list_list])
    ax.set_xticklabels(tick_labels, rotation=30, ha="right")
    ax.yaxis.grid(True)
    ax.set_axisbelow(True)
    plt.tight_layout(rect=[0.02, -.05, 1, .95])
    ax.set_ylim(0.9)
    plt.yticks([0.9,0.95,1,1.05,1.1])
    ax.yaxis.set_major_formatter(FormatStrFormatter('%g'))
    ax.set_ylabel("Normalized Execution Time")

    fig.suptitle(f"Overhead for different Kernels using EZTrace/OTF2.")
    fig.savefig(f"OTF2 All Kernel Overhead.svg", dpi=300)
