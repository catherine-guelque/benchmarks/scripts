#!/bin/python
# Imports
import json
import re
import sys
from operator import attrgetter
from typing import List

import matplotlib.pyplot as plt
import os

from matplotlib.patches import Patch, FancyBboxPatch
from matplotlib.patches import Rectangle

import numpy as np

colors = {
    "OTF2": "tab:blue",
    "Pallas": "tab:orange",
    "Pilgrim": "tab:green"
}


def getColor(attribute: str):
    for tool in colors.keys():
        if tool.upper() == attribute.upper():
            return colors[tool]


rewriteKeys = {
    "pallas": "Pallas",
    "otf2": "OTF2",
    "pilgrim": "Pilgrim"
}

kernel_tracingTool_nMachines_data = {}


def plot_kernel(kernel: str):
    print(f"Plotting kernel: {kernel}")
    tracingTool_nMachines_data = kernel_tracingTool_nMachines_data[kernel]
    fig, ax = plt.subplots(figsize=(12, 8))
    tracingTool_list = list(tracingTool_nMachines_data.keys())
    tracingTool_list.sort()
    categories = list(tracingTool_nMachines_data["pallas"].keys())
    categories.sort()
    n_categories = len(categories)
    categories_x_list = np.arange(n_categories)
    column_width = 0.6 / len(tracingTool_list)
    std_factor = 1
    # Interval de confiance à 99%

    min_value = np.inf
    max_value = 0
    values = {}
    print(tracingTool_nMachines_data)
    wantToPlot = "duration"
    # wantToPlot = "memory"
    # wantToPlot = "memory_percent"
    for (tracingTool, nMachines_data) in tracingTool_nMachines_data.items():
        for (nMachines, data) in nMachines_data.items():
            plotted = None
            if wantToPlot == "memory_percent":
                plotted = np.array(data["memory"]) / np.array(data["trace_size"]) * 100
            else:
                plotted = data[wantToPlot]
            if len(plotted) == 0:
                plotted = [0]
            mean = np.mean(plotted)
            catIndex = categories.index(nMachines)
            subCatIndex = tracingTool_list.index(tracingTool)
            ax.bar(
                [categories_x_list[catIndex] + column_width * subCatIndex],
                [mean],
                tick_label=tracingTool,
                yerr=std_factor * np.std(plotted),
                capsize=40 * column_width,
                width=column_width,
                # label=tracingTool,
                color=getColor(rewriteKeys[tracingTool])
            )
    ax.set_xticks(categories_x_list)
    ax.set_xticklabels(categories)
    ax.yaxis.grid(True)
    ax.set_axisbelow(True)
    # ax.set_ylim([min_value * 0.9, max_value * 1.1])
    plt.tight_layout(rect=[0.01, 0.01, 1, .85])

    ax.set_xlabel("Number of threads in the trace")

    color_boxes = [Patch(label=k, color=v) for k, v in colors.items()]
    color_legend = fig.legend(handles=color_boxes,
                              loc="upper center",
                              bbox_to_anchor=(0.5, .87),
                              ncol=4,
                              fontsize="16")
    ax.add_artist(color_legend)
    if wantToPlot == "duration":
        ax.set_ylabel("Execution time (s)")
        fig.suptitle(f"Time to make a communication matrix for {kernel.capitalize()}")
    elif wantToPlot == "memory_percent":
        ax.set_ylabel("% of the trace size consumed as memory")
        fig.suptitle(f"Relative memory required to plot a communication matrix for {kernel.capitalize()}", wrap=True)
    else:
        ax.set_ylabel("Memory consumed (GB)")
        fig.suptitle(f"Memory required to plot a communication matrix for {kernel.capitalize()}", wrap=True)

    fig.savefig(f"analysisgraph/{kernel}_{wantToPlot}.svg", dpi=300)
    # plt.show()
    for tracingTool in values:
        if tracingTool == "vanilla":
            print(f"{kernel} Vanilla timing: {values['vanilla']}")
        else:
            print(
                f"{kernel} {tracingTool}: {(values[tracingTool] - values['vanilla']) * 100.0 / values['vanilla']}")


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: CompareCompression <ExperimentName>")
        print("With <ExperimentName>_timedata.json is a file that exists.")
        exit(1)
    experimentName = sys.argv[1]
    with open(f"{experimentName}_analysisdata.json", "r") as infile:
        tempDict = json.load(infile)
        for (traceName, analysisPerfs) in tempDict["comm_matrix"].items():
            # pilgrim/minife/NO_COMP/2024-04-04T18_34_26.645/mpi_medium_160_2_trace
            (name, duration, memory, trace_size) = analysisPerfs.split(',')
            path = traceName.split('/')
            tracingTool = "otf2" if "otf2" in traceName else "pallas" if "pallas" in traceName else "pilgrim"
            path = path[path.index(tracingTool):]
            kernel = path[1]
            compression = path[2] if tracingTool != "otf2" else None
            trace_name = path[-1]
            nMachines = int(trace_name.split("_")[2])
            if kernel not in kernel_tracingTool_nMachines_data:
                kernel_tracingTool_nMachines_data[kernel] = {}
            # TODO trier par taille
            if tracingTool not in kernel_tracingTool_nMachines_data[kernel]:
                kernel_tracingTool_nMachines_data[kernel][tracingTool] = {}
            if nMachines not in kernel_tracingTool_nMachines_data[kernel][tracingTool]:
                kernel_tracingTool_nMachines_data[kernel][tracingTool][nMachines] = {
                    "duration": [],
                    "memory": [],
                    "trace_size": []
                }
            if duration != "":
                kernel_tracingTool_nMachines_data[kernel][tracingTool][nMachines]["duration"].append(float(duration))
            if memory != "":
                kernel_tracingTool_nMachines_data[kernel][tracingTool][nMachines]["memory"].append(float(memory))
            if trace_size != "":
                kernel_tracingTool_nMachines_data[kernel][tracingTool][nMachines]["trace_size"].append(float(trace_size))

    os.makedirs(f"analysisgraph", exist_ok=True)
    plt.rcParams.update({'font.size': 30})
    plt.rcParams.update({'mathtext.fontset': 'stix'})
    plt.rcParams.update({'font.family': 'STIXGeneral'})
    for kernel in kernel_tracingTool_nMachines_data:
        plot_kernel(kernel)
