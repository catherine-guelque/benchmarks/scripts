#!/bin/python
# Imports
import json
import re
import sys
from operator import attrgetter

import matplotlib.pyplot as plt
import os

from matplotlib.patches import Patch, FancyBboxPatch
from matplotlib.patches import Rectangle

import numpy as np

colors = {
    "OTF2": "tab:blue",
    "Pallas": "tab:orange",
    "Pilgrim": "tab:green"
}

def getColor(attribute:str):
    for tool in colors.keys():
        if tool.upper() == attribute.upper():
            return colors[tool]

style = {
    'No Compression': '',
    'ZSTD': '/',
    'SZ': '-',
    'ZFP': '\\'
}

kernel_tracingTool_size_nMachines_dict: dict[str, dict[str, dict[str, dict[int, list[int]]]]] = {}

def plot_kernel(kernel:str):
    print(f"Plotting kernel: {kernel}")
    tracingTool_size_nMachines_dict = kernel_tracingTool_size_nMachines_dict[kernel]
    availableTracingTools = list(tracingTool_size_nMachines_dict.keys())
    availableTracingTools.sort()
    n_tracing_tool = len(tracingTool_size_nMachines_dict)
    fig, ax = plt.subplots(figsize=(12, 8))

    available_size = []
    for tracingTool in tracingTool_size_nMachines_dict:
        for size in tracingTool_size_nMachines_dict[tracingTool]:
            if size not in available_size:
                available_size.append(size)
    for size in available_size:
        available_nMachines = []

        for tracingTool in availableTracingTools:
            for size in tracingTool_size_nMachines_dict[tracingTool]:
                for n_machines in tracingTool_size_nMachines_dict[tracingTool][size]:
                    if int(n_machines) not in available_nMachines:
                        available_nMachines.append(int(n_machines))
        available_nMachines.sort()
        n_categories = len(available_nMachines)
        n_subcategories = len(availableTracingTools)

        categories_x_list = np.arange(n_categories)
        subcategories_x_list = np.linspace(0, 1, num=n_subcategories + 2)
        column_width = 1 / (n_subcategories + 1)
        std_factor = 1
        # Interval de confiance à 99%

        for tracingTool in tracingTool_size_nMachines_dict:
            if tracingTool == "vanilla":
                continue
            compression_tool = re.search(r'(ZSTD|SZ|ZFP)', tracingTool)
            compression_tool = compression_tool.group() if compression_tool else 'No Compression'
            compression_tool_index = list(style.keys()).index(compression_tool)
            raw_tracingTool = tracingTool.split("_")[0]
            for nMachine in tracingTool_size_nMachines_dict[tracingTool][size]:
                trace_size = np.array(tracingTool_size_nMachines_dict[tracingTool][size][nMachine]) / (1024 * 1024)
                catIndex = available_nMachines.index(int(nMachine))
                subcatIndex = availableTracingTools.index(tracingTool)
                mean = np.mean(trace_size)
                ax.bar(
                    [categories_x_list[catIndex] + subcategories_x_list[subcatIndex]],
                    [mean],
                    yerr=std_factor * np.std(trace_size),
                    capsize=40 * column_width,
                    width=column_width,
                    label=raw_tracingTool,
                    color=getColor(raw_tracingTool),
                    hatch=style[compression_tool]
                )
        ax.set_xticks(categories_x_list + np.mean(subcategories_x_list[:-1]), available_nMachines)
        ax.yaxis.grid(True)
        ax.set_axisbelow(True)
        plt.yscale('log')
        plt.tight_layout(rect=[0.01, 0.01, 1, .85])

        # ax.set_ylabel("Taille de la trace (Mio)")
        # ax.set_xlabel("Type de test")
        ax.set_ylabel("Trace size (MiB)")
        ax.set_xlabel("Number of processes")

        color_boxes = [Patch(label=k, color=v) for k, v in colors.items()]
        color_legend = fig.legend(handles=color_boxes,
                                  loc="upper center",
                                  bbox_to_anchor=(0.5, .92),
                                  fancybox=False, frameon=False,
                                  ncol=4,
                                  fontsize="16")
        marker_lines = [Patch(fc="white", ec="black", linewidth=1, label=k, hatch=v) for k, v in style.items()]
        marker_legend = fig.legend(handles=marker_lines,
                                   loc="upper center",
                                   bbox_to_anchor=(0.5, .87),
                                   fancybox=False, frameon=False,
                                   ncol=4,
                                   fontsize="16")
        ax.add_artist(color_legend)
        bb1 = color_legend.get_window_extent().transformed(fig.transFigure.inverted())
        bb2 = marker_legend.get_window_extent().transformed(fig.transFigure.inverted())
        # Calculate the encompassing bounding box (xmin, ymin, xmax, ymax)
        padding = -0.015  # Adjust padding to your liking
        xmin = min(bb1.x0, bb2.x0) - padding
        ymin = min(bb1.y0, bb2.y0) - padding
        xmax = max(bb1.x1, bb2.x1) + padding
        ymax = max(bb1.y1, bb2.y1) + padding

        rect = FancyBboxPatch((xmin, ymin), xmax-xmin, ymax-ymin,
                              boxstyle="round,pad=0.02", ec="black", fc="none",
                              transform=fig.transFigure, lw=1, zorder=2)

        # Add the rectangle to the figure
        fig.patches.append(rect)

        fig.suptitle(f"Size of traces for {kernel.capitalize()} - {size.capitalize()}")
        fig.savefig(f"sizegraph/{kernel}_{size}.svg", dpi=300)
        # plt.show()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: CompareCompression <ExperimentName>")
        print("With <ExperimentName>_sizedata.json is a file that exists.")
        exit(1)
    experimentName = sys.argv[1]
    with open(f"{experimentName}_sizedata.json", "r") as infile:
        kernel_tracingTool_size_nMachines_dict = json.load(infile)
    os.makedirs(f"sizegraph", exist_ok=True)
    plt.rcParams.update({'font.size': 30})
    plt.rcParams.update({'mathtext.fontset': 'stix'})
    plt.rcParams.update({'font.family': 'STIXGeneral'})
    for kernel in kernel_tracingTool_size_nMachines_dict:
        plot_kernel(kernel)
