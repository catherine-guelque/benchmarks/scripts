#!/bin/python
# Imports
import json
import sys
import matplotlib.pyplot as plt
from matplotlib.patches import Patch, FancyBboxPatch
import numpy as np
from matplotlib.offsetbox import OffsetImage, AnnotationBbox

colors = {
    "OTF2": "tab:blue",
    "Pallas": "tab:orange",
    "Pilgrim": "tab:green"
}


def getColor(attribute: str):
    if attribute == "vanilla":
        return colors["No Tracing"]
    for tool in colors.keys():
        if tool.upper() == attribute.upper():
            return colors[tool]


rewriteKeys = {
    "vanilla": "No tracing",
    "pallas_NO_COMP": "Pallas Vanilla",
    "pallas_ZSTD": "Pallas ZSTD",
    "pallas_ZFP": "Pallas ZFP",
    "otf2": "OTF2"
}

kernel_tracingTool_size_nMachines_dict: dict[str, dict[str, dict[str, dict[str, list[int]]]]] = {}
dead_puter = plt.imread('red_cross.png')
dead_puter_box = OffsetImage(dead_puter, zoom=.04)
max_value = 100
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: CompareCompression <ExperimentName>")
        print("With <ExperimentName>_timedata.json is a file that exists.")
        exit(1)
    experimentName = sys.argv[1]
    with open(f"{experimentName}_timedata.json", "r") as infile:
        kernel_tracingTool_size_nMachines_dict = json.load(infile)
    plt.rcParams.update({'font.size': 30})
    plt.rcParams.update({'mathtext.fontset': 'stix'})
    plt.rcParams.update({'font.family': 'STIXGeneral'})
    figHeight, figwWidth = 10, 20
    fig, ax = plt.subplots(figsize=(figwWidth, figHeight))
    blacklist = ["amg"]
    list_kernel = [k for k in kernel_tracingTool_size_nMachines_dict.keys() if k not in blacklist]
    n_categories = len(list_kernel)
    categories_x_list = np.arange(n_categories)
    subcategories_x_list_list = []
    tick_labels = []
    for kernel in list_kernel:
        print(f"Plotting kernel: {kernel}")
        tracingTool_size_nMachines_dict = kernel_tracingTool_size_nMachines_dict[kernel]
        # availableTracingTools = list(tracingTool_size_nMachines_dict.keys())
        availableTracingTools = ['otf2','pallas_ZSTD', 'pilgrim_ZSTD']
        n_tracing_tool = len(availableTracingTools)

        available_size = []
        for tracingTool in availableTracingTools:
            if tracingTool not in tracingTool_size_nMachines_dict:
                continue
            for size in tracingTool_size_nMachines_dict[tracingTool]:
                if size not in available_size:
                    available_size.append(size)
        size = available_size[-1]
        del available_size
        available_nMachines = []
        for tracingTool in availableTracingTools:
            if tracingTool not in tracingTool_size_nMachines_dict:
                continue
            for nMachine in tracingTool_size_nMachines_dict[tracingTool][size]:
                if int(nMachine) not in available_nMachines:
                    available_nMachines.append(int(nMachine))
        nMachine = max(available_nMachines)
        del available_nMachines

        n_subcategories = len(availableTracingTools)
        subcategories_x_list = np.linspace(0, 1, num=n_subcategories + 2)
        subcategories_x_list_list.append(subcategories_x_list)
        column_width = 1 / (n_subcategories + 1)
        std_factor = 1
        tick_labels.append(f"{kernel.upper().replace('_', ' ')}" if "nas" in kernel else kernel.capitalize())
        tick_labels[-1] += f" {nMachine}"
        # Interval de confiance à 99%
        vanilla_timings_mean = np.mean([t for t in tracingTool_size_nMachines_dict["vanilla"][size][str(nMachine)] if t])
        for tracingTool in availableTracingTools:
            raw_tracingTool = tracingTool.split("_")[0]
            catIndex = list_kernel.index(kernel)
            subcatIndex = availableTracingTools.index(tracingTool)
            if tracingTool not in tracingTool_size_nMachines_dict:
                x = categories_x_list[catIndex] + subcategories_x_list[subcatIndex] +.05
                ab = AnnotationBbox(dead_puter_box, (x, 5), frameon=False)
                ax.add_artist(ab)
                continue
            trace_size = np.array([t for t in tracingTool_size_nMachines_dict[tracingTool][size][str(nMachine)] if t])
            if len(trace_size) == 0:
                x = categories_x_list[catIndex] + subcategories_x_list[subcatIndex] +.05
                ab = AnnotationBbox(dead_puter_box, (x, 5), frameon=False)
                ax.add_artist(ab)
                continue
            mean = (np.mean(trace_size) / vanilla_timings_mean - 1) * 100
            col_x = categories_x_list[catIndex] + subcategories_x_list[subcatIndex]
            if mean > max_value:
                ax.text(s=f"{int(mean)}%",
                        x=col_x - column_width / 4,
                        y=82,
                        color="white",
                        rotation="vertical"
                        )
            ax.bar(
                [col_x],
                [mean],
                yerr=std_factor * np.std(trace_size) / vanilla_timings_mean,
                capsize=40 * column_width,
                width=column_width,
                label=raw_tracingTool,
                color=getColor(raw_tracingTool)
            )
    ax.set_xticks(categories_x_list + [np.mean(s[:-1]) for s in subcategories_x_list_list] - column_width / 2)
    ax.set_xticklabels(tick_labels, rotation=30, ha="right")
    ax.yaxis.grid(True)
    ax.set_axisbelow(True)
    plt.tight_layout(rect=[0.04, -.05, 1, .88])
    yticks = [0, 20, 40, 60, 80, 100]
    ax.set_yticks(yticks)
    ax.set_yticklabels([f"{t}%" for t in yticks])
    ax.set_ylim([0, max_value])
    ax.set_ylabel("Overhead")

    color_boxes = [Patch(label=k, color=v) for k, v in colors.items()] + [ Patch(label="Aborted Execution", color="none") ]
    color_legend = fig.legend(handles=color_boxes,
                              loc="upper center",
                              bbox_to_anchor=(0.5, .92),
                              fancybox=False, frameon=False,
                              ncol=4,
                              fontsize="20")
    ab = AnnotationBbox(dead_puter_box, (.7, 0.55), xycoords=color_legend, frameon=False)
    ax.add_artist(color_legend)
    ax.add_artist(ab)
    bb1 = color_legend.get_window_extent().transformed(fig.transFigure.inverted())
    bb2 = ab.get_window_extent().transformed(fig.transFigure.inverted())
    padding = -0.015  # Adjust padding to your liking
    xmin = bb1.x0 - padding
    ymin = bb2.y0 - 2 * padding
    xmax = bb1.x1 + padding
    ymax = bb2.y1 + 2 * padding

    rect = FancyBboxPatch((xmin, ymin), xmax - xmin, ymax - ymin,
                          boxstyle="round,pad=0.02", ec="black", fc="none",
                          transform=fig.transFigure, lw=1, zorder=2)
    # Add the rectangle to the figure
    fig.patches.append(rect)

    for temp in [color_legend.get_bbox_to_anchor(), color_legend.get_window_extent(), color_legend.get_clip_box()]:
        print(temp.x0, temp.y0)

    fig.suptitle(f"Overhead for different Kernels.")
    fig.savefig(f"All Kernel Overhead.svg", dpi=300)
