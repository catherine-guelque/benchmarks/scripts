#!/bin/python
# Imports
import json
import re
import sys
from operator import attrgetter
from typing import List

import matplotlib.pyplot as plt
import os

from matplotlib.patches import Patch, FancyBboxPatch
from matplotlib.patches import Rectangle

import numpy as np

colors = {
    "OTF2": "tab:blue",
    "Pallas": "tab:orange",
    "Pilgrim": "tab:green"
}


def getColor(attribute: str):
    for tool in colors.keys():
        if tool.upper() == attribute.upper():
            return colors[tool]


rewriteKeys = {
    "pallas": "Pallas",
    "otf2": "OTF2",
    "pilgrim": "Pilgrim"
}

blacklist = ["nas_ep", "nas_ft", "nas_is", "kripke"]


def plotDuration(data: dict, title: str, name: str, includeOTF2: bool):
    kernel_nProc_tool_data = {}
    for fullPath in data:
        analysisData = data[fullPath].split(',')
        fullPath = fullPath.split('/')
        traceName = fullPath[-1]
        nProc = int(traceName.split('_')[-3])
        kernel: str = fullPath[-3 if "otf2" in fullPath else -4]
        if kernel in blacklist:
            continue
        compression: str = None if "otf2" in fullPath else fullPath[-3]
        tool = "otf2" if "otf2" in fullPath else "pallas" if "pallas" in fullPath else "pilgrim"
        if not includeOTF2 and tool == "otf2":
            continue
        if compression and compression != "NO_COMP":
            continue
        if "nas" in kernel:
            kernel = kernel.replace('_', ' ').upper()
        else:
            kernel = kernel.capitalize()
        if not kernel in kernel_nProc_tool_data:
            kernel_nProc_tool_data[kernel] = {}
        if not nProc in kernel_nProc_tool_data[kernel]:
            kernel_nProc_tool_data[kernel][nProc] = {"otf2": [], "pallas": [], "pilgrim": []}
        kernel_nProc_tool_data[kernel][nProc][tool].append(float(analysisData[1]))

    fig, ax = plt.subplots(figsize=(12, 8))
    categories = list(kernel_nProc_tool_data.keys())
    categories.sort()
    n_categories = len(categories)
    categories_x_list = np.arange(n_categories)
    subcategories = ["otf2", "pallas", "pilgrim", ]
    if not includeOTF2:
        subcategories.remove("otf2")
    column_width = 0.6 / (len(subcategories) + 1)
    std_factor = 1
    # Interval de confiance à 99%

    for kernel in kernel_nProc_tool_data:
        nProc = max(kernel_nProc_tool_data[kernel].keys())
        for tool in subcategories:
            mean = np.mean(kernel_nProc_tool_data[kernel][nProc][tool])
            catIndex = categories.index(kernel)
            ax.bar(
                [categories_x_list[catIndex] + column_width * subcategories.index(tool)],
                [mean],
                yerr=std_factor * np.std(kernel_nProc_tool_data[kernel][nProc][tool]),
                capsize=20 * column_width,
                width=column_width,
                color=getColor(rewriteKeys[tool])
            )
    ax.set_xticks(categories_x_list + column_width)
    categories = [f"{c} {max(kernel_nProc_tool_data[c].keys())}" for c in categories]
    ax.set_xticklabels(categories, rotation=30, ha="right")
    ax.yaxis.grid(True)
    ax.set_axisbelow(True)
    plt.tight_layout(rect=(0, -0.03, 1, .82))
    color_boxes = [Patch(label=k, color=v) for k, v in colors.items() if k != "OTF2" or includeOTF2]
    color_legend = fig.legend(handles=color_boxes,
                              loc="upper center",
                              bbox_to_anchor=(0.5, .86),
                              fancybox=False,
                              ncol=4,
                              fontsize="20")

    ax.add_artist(color_legend)
    ax.set_ylabel("Execution time (s)")
    fig.suptitle(title)
    fig.savefig(name, dpi=300)


def plotMemory(data: dict, title: str, name: str, includeOTF2: bool):
    kernel_nProc_data = {}
    for fullPath in data:
        analysisData = data[fullPath].split(',')
        fullPath = fullPath.split('/')
        traceName = fullPath[-1]
        nProc = int(traceName.split('_')[-3])
        kernel: str = fullPath[-3 if "otf2" in fullPath else -4]
        if kernel in blacklist:
            continue
        compression: str = None if "otf2" in fullPath else fullPath[-3]
        tool = "otf2" if "otf2" in fullPath else "pallas" if "pallas" in fullPath else "pilgrim"
        if not includeOTF2 and tool == "otf2":
            continue
        if "nas" in kernel:
            kernel = kernel.replace('_', ' ').upper()
        else:
            kernel = kernel.capitalize()
        if not kernel in kernel_nProc_data:
            kernel_nProc_data[kernel] = {}
        if not nProc in kernel_nProc_data[kernel]:
            kernel_nProc_data[kernel][nProc] = {"otf2": [], "pallas": [], "pilgrim": []}
        kernel_nProc_data[kernel][nProc][tool].append(float(analysisData[2]))

    fig, ax = plt.subplots(figsize=(12, 8))
    categories = list(kernel_nProc_data.keys())
    categories.sort()
    n_categories = len(categories)
    categories_x_list = np.arange(n_categories)
    subcategories = ["otf2", "pallas", "pilgrim"]
    if not includeOTF2:
        subcategories.remove("otf2")
    column_width = 0.6 / (len(subcategories) + 1)
    std_factor = 1
    # Interval de confiance à 99%

    for kernel in kernel_nProc_data:
        nProc = max(kernel_nProc_data[kernel].keys())
        for tool in subcategories:
            mean = np.mean(kernel_nProc_data[kernel][nProc][tool])
            catIndex = categories.index(kernel)
            ax.bar(
                [categories_x_list[catIndex] + column_width * subcategories.index(tool)],
                [mean],
                yerr=std_factor * np.std(kernel_nProc_data[kernel][nProc][tool]),
                capsize=20 * column_width,
                width=column_width,
                color=getColor(rewriteKeys[tool])
            )
    ax.set_xticks(categories_x_list + column_width)
    categories = [f"{c} {max(kernel_nProc_data[c].keys())}" for c in categories]
    ax.set_xticklabels(categories, rotation=30, ha="right")
    ax.yaxis.grid(True)
    ax.set_axisbelow(True)
    plt.tight_layout(rect=(0, -0.03, 1, .82))
    color_boxes = [Patch(label=k, color=v) for k, v in colors.items() if k != "OTF2" or includeOTF2]
    color_legend = fig.legend(handles=color_boxes,
                              loc="upper center",
                              bbox_to_anchor=(0.5, .86),
                              fancybox=False,
                              ncol=4,
                              fontsize="20")

    ax.add_artist(color_legend)

    ax.set_ylabel("Memory usage (GiB)")
    fig.suptitle(title)
    fig.savefig(name, dpi=300)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: OTF2Analysis <ExperimentName>")
        print("With <ExperimentName>_analysisdata.json is a file that exists.")
        exit(1)
    experimentName = sys.argv[1]
    comMatrixData: dict
    contentionData: dict
    with open(f"{experimentName}_analysisdata.json", "r") as infile:
        data = json.load(infile)
        comMatrixData = data["comm_matrix"]
        contentionData = data["contention"]
    plt.rcParams.update({'font.size': 30})
    plt.rcParams.update({'mathtext.fontset': 'stix'})
    plt.rcParams.update({'font.family': 'STIXGeneral'})
    figHeight, figwWidth = 10, 20
    fig, ax = plt.subplots(figsize=(figwWidth, figHeight))
    plotDuration(comMatrixData,
                 f"Time to plot a communication matrix\nfrom different trace formats.",
                 f"All Kernel Matrix Time.svg",
                 True)
    plotMemory(comMatrixData,
               f"Memory consumption to plot a communication matrix\nfrom different trace formats.",
               "All Kernel Matrix Memory.svg",
               True)
    plotDuration(comMatrixData,
                 f"Time to plot a communication matrix\nfrom different trace formats.",
                 f"All Kernel Matrix Time No OTF2.svg",
                 False)
    plotMemory(comMatrixData,
               f"Memory consumption to plot a communication matrix\nfrom different trace formats.",
               "All Kernel Matrix Memory No OTF2.svg",
               False)
    plotMemory(contentionData,
               f"Memory consumption to detect contention\nfrom different traces.",
               "All Kernel Contention Memory.svg",
               True)
    # plotContention(contentionData)
