#!/bin/python
# Imports
import json
import re
import sys
from operator import attrgetter

import matplotlib.pyplot as plt
import os

from matplotlib.patches import Patch, FancyBboxPatch
from matplotlib.patches import Rectangle

import numpy as np
from reportlab.lib.colors import black

colors = {
    # "No Tracing": "tab:gray",
    "OTF2": "tab:blue",
    "Pallas": "tab:orange",
    "Pilgrim": "tab:green"
}


def getColor(attribute: str):
    if attribute == "vanilla":
        return colors["No Tracing"]
    for tool in colors.keys():
        if tool.upper() == attribute.upper():
            return colors[tool]


style = {
    'No Compression': '',
    'ZSTD': '/',
    'SZ': '-',
}

kernel_tracingTool_size_nMachines_dict: dict[str, dict[str, dict[str, dict[int, list[int]]]]] = {}
rewriteKeys = {
    "vanilla": "No tracing",
    "pallas_NO_COMP": "Pallas Vanilla",
    "pallas_ZSTD": "Pallas ZSTD",
    "pallas_SZ": "Pallas SZ",
    "pilgrim_NO_COMP": "Pilgrim Vanilla",
    "pilgrim_ZSTD": "Pilgrim ZSTD",
    "pilgrim_SZ": "Pilgrim SZ",
    "otf2": "OTF2"
}
blacklist = ["amg", "lulesh"]
x_limit = 100


def plot_kernel(kernel: str):
    print(f"Plotting kernel: {kernel}")
    tracingTool_size_nMachines_dict = kernel_tracingTool_size_nMachines_dict[kernel]
    availableTracingTools = ['otf2', 'pallas_ZSTD', 'pallas_SZ', 'pilgrim_ZSTD', 'pilgrim_SZ']
    fig, ax = plt.subplots(figsize=(12, 8))

    available_size = []
    for tracingTool in availableTracingTools:
        for size in tracingTool_size_nMachines_dict[tracingTool]:
            if size not in available_size:
                available_size.append(size)
    for size in available_size:
        available_nMachines = []
        for tracingTool in availableTracingTools:
            for size in tracingTool_size_nMachines_dict[tracingTool]:
                for n_machines in tracingTool_size_nMachines_dict[tracingTool][size]:
                    if int(n_machines) not in available_nMachines:
                        available_nMachines.append(int(n_machines))
        available_nMachines.sort()
        n_categories = len(available_nMachines)
        categories_x_list = np.arange(n_categories)
        n_subcategories = len(availableTracingTools)
        subcategories_x_list_list = []
        tick_labels = []

        min_value = np.inf
        max_value = 0
        for nMachine in available_nMachines:
            subcategories_x_list = np.linspace(0, 1, num=n_subcategories + 2)
            subcategories_x_list_list.append(subcategories_x_list)
            column_width = 1 / (n_subcategories + 1)
            std_factor = 1
            # tick_labels.append(f"{kernel.upper().replace('_', ' ')}" if "nas" in kernel else kernel.capitalize())
            tick_labels.append(f"{nMachine}")
            vanilla_timings_mean = np.mean(
                [t for t in tracingTool_size_nMachines_dict["vanilla"][size][str(nMachine)] if t])
            for tracingTool in availableTracingTools:
                compression_tool = re.search(r'(ZSTD|SZ|ZFP)', tracingTool)
                compression_tool = compression_tool.group() if compression_tool else 'No Compression'
                compression_tool_index = list(style.keys()).index(compression_tool)
                raw_tracingTool = tracingTool.split("_")[0]
                catIndex = available_nMachines.index(nMachine)
                subcatIndex = availableTracingTools.index(tracingTool)
                trace_size = np.array(
                    [t for t in tracingTool_size_nMachines_dict[tracingTool][size][str(nMachine)] if t])
                mean = np.mean(trace_size)
                mean = (np.mean(trace_size) / vanilla_timings_mean - 1) * 100

                min_value = min(min_value, mean)
                max_value = max(max_value, mean)

                col_x = categories_x_list[catIndex] + subcategories_x_list[subcatIndex]
                if mean > x_limit:
                    ax.text(s=f"{int(mean)}%",
                            x=col_x - column_width / 4,
                            y=70,
                            color="white",
                            rotation="vertical"
                            )
                ax.bar(
                    [col_x],
                    [mean],
                    tick_label=tracingTool,
                    yerr=std_factor * np.std(trace_size),
                    capsize=40 * column_width,
                    width=column_width,
                    label=raw_tracingTool,
                    color=getColor(raw_tracingTool),
                    hatch=style[compression_tool]
                )
        ax.set_xticks(categories_x_list)
        ax.set_xticklabels(tick_labels, rotation=30, ha="right")
        ax.yaxis.grid(True)
        ax.set_axisbelow(True)
        ax.set_ylim([0, min(x_limit, max_value * 1.2)])
        # plt.yscale('log')
        plt.tight_layout(rect=[0.01, 0.01, 1, .85])

        # ax.set_ylabel("Taille de la trace (Mio)")
        # ax.set_xlabel("Type de test")
        ax.set_ylabel("Overhead (%)")

        color_boxes = [Patch(label=k, color=v) for k, v in colors.items()]
        color_legend = fig.legend(handles=color_boxes,
                                  loc="upper center",
                                  bbox_to_anchor=(0.5, .92),
                                  fancybox=False, frameon=False,
                                  ncol=4,
                                  fontsize="16")
        ax.add_artist(color_legend)
        marker_lines = [Patch(fc="white", ec="black", linewidth=1, label=k, hatch=v) for k, v in style.items()]
        marker_legend = fig.legend(handles=marker_lines,
                                   loc="upper center",
                                   bbox_to_anchor=(0.5, .87),
                                   fancybox=False, frameon=False,
                                   ncol=4,
                                   fontsize="16")
        ax.add_artist(color_legend)
        bb1 = color_legend.get_window_extent().transformed(fig.transFigure.inverted())
        bb2 = marker_legend.get_window_extent().transformed(fig.transFigure.inverted())
        # Calculate the encompassing bounding box (xmin, ymin, xmax, ymax)
        padding = -0.015  # Adjust padding to your liking
        xmin = min(bb1.x0, bb2.x0) - padding
        ymin = min(bb1.y0, bb2.y0) - padding
        xmax = max(bb1.x1, bb2.x1) + padding
        ymax = max(bb1.y1, bb2.y1) + padding

        rect = FancyBboxPatch((xmin, ymin), xmax - xmin, ymax - ymin,
                              boxstyle="round,pad=0.02", ec="black", fc="none",
                              transform=fig.transFigure, lw=1, zorder=2)

        # Add the rectangle to the figure
        fig.patches.append(rect)

        fig.suptitle(f"Overhead for {kernel.capitalize()} - {size.capitalize()}")
        if "nas" in kernel:
            fig.suptitle(
                f"Overhead for {kernel.upper().replace('_', ' ')} - {size.capitalize()}")
        fig.savefig(f"timegraph/{kernel}_{size}.svg", dpi=300)
        # plt.show()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: CompareCompression <ExperimentName>")
        print("With <ExperimentName>_timedata.json is a file that exists.")
        exit(1)
    experimentName = sys.argv[1]
    with open(f"{experimentName}_timedata.json", "r") as infile:
        kernel_tracingTool_size_nMachines_dict = json.load(infile)
    os.makedirs(f"timegraph", exist_ok=True)
    plt.rcParams.update({'font.size': 30})
    plt.rcParams.update({'mathtext.fontset': 'stix'})
    plt.rcParams.update({'font.family': 'STIXGeneral'})
    for kernel in kernel_tracingTool_size_nMachines_dict:
        if kernel in blacklist:
            continue
        plot_kernel(kernel)
