# Scripts
This is a simple Git project for easy benchmarking.
Explanation for everything:
 - `benchmarks`: Contains all the benchmarks. `cloneBenchmarks.sh`, `buildBenchmarks.sh` and `cleanBenchmarks.sh`
are here to respectively clone, build and clean those benchmarks.
 - `outputLogs`: Contains all the output logs, in neatly sorted folders. These also contain Python scripts for making the plots.
 - `software`: Contains `installAll.sh`, a script that download and builds a bunch of pre-required libraries for everything.
Also builds and install Pallas, Pilgrim and ScalaTrace.

## Installing

```
git clone https://gitlab.inf.telecom-sudparis.eu/catherine-guelque/benchmarks/scripts.git
cd scripts/software
./installAll.sh
source base_software.env
cd ../benchmarks
./cloneBenchmarks.sh
./buildBenchmarks.sh
```

Note that, on grid5000, you will need to load the following modules:
- `intel-oneapi-mkl`
- `opa-psm2`

As well as load the guix manifest (`guix shell` in `software` dir) 
## Benchmarking scripts
 - `horizontalScaling`: generate traces with OTF2, Pallas and Pilgrim with horizontal scaling.
 - `horizontalScalingCompression`: generate traces with Pallas and Pilgrim with horizontal scaling, and compression.
You should give the compression tool you want to use as its first argument.
 - `verticalScaling`: generate traces with OTF2, Pallas and Pilgrim with vertical scaling.
 - `verticalScalingCompression`: generate traces with Pallas and Pilgrim with vertical scaling, and compression.
  You should give the compression tool you want to use as its first argument.

## Tests scripts (WIP)
 - `Analysis`: Shows the time needed to get some info from a trace for OTF2, Pallas and Pilgrim
 - `Traces`: Tests the validity of the traces.


## Visualising
Each folder in `outputLogs` has some Python scripts that will generate plots.


## Running on Jean-Zay

```
PROJECT_DIR=/gpfswork/rech/avz/commun
cd $PROJECT_DIR
module load cmake  starpu/1.4.2-mpi-cuda-debug gcc/8.5.0 cuda/11.2 intel-mpi/2021.9 hwloc/2.7.1 libpciaccess/0.16 libxml2/2.9.9
export CC=icc
export CXX=icpc

```